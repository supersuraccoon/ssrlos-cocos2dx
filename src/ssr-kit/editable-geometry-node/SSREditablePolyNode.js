/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.EditablePolyNode = ssr.EditableGeometryNode.extend({
    ctor:function(vertexArray, isPolygon) {
        this._isPolygon = (isPolygon === undefined ? true : isPolygon);
        this._super(vertexArray);
    },
    /* 
        渲染多边形
        只需要在顶点发生变化和位置发生变化的时候调用，任何形变都不需要调用
    */
    _render:function() {
        // render
        this._renderNode.clear();
        this._renderNode.setPosition(0, 0);
        if (this._isPolygon) {
            if  (ssr.PolyHelper.isConvex(this._inNodeVertextArray)) {
                this._renderNode.drawPoly(this._inNodeVertextArray, this._renderColor, cc.FLT_EPSILON, cc.color(0, 0, 0, 0));
            }
            else {
                var triangulatePolys = ssr.PolyHelper.triangulate(this._inNodeVertextArray);
                for (var t = 0, tl = triangulatePolys.length / 3; t < tl; t ++) {
                    var triangle = [triangulatePolys[t * 3], triangulatePolys[t * 3 + 1], triangulatePolys[t * 3 + 2]];
                    this._renderNode.drawPoly(triangle, this._renderColor, cc.FLT_EPSILON, cc.color(0, 0, 0, 0));
                }
            }
        }
        else {
            for (var i = 0, l = this._inNodeVertextArray.length; i < l - 1; i ++) {
                this._renderNode.drawSegment(this._inNodeVertextArray[i], this._inNodeVertextArray[i + 1], 2, this._renderColor);
            }
        }
        // debug
        if (this._isDebugDrawOn) {
            this._debugDraw();
        }
    },
    _debugDraw:function() {
        this._super();
        // vertex
        this._debugLabelNode.setPosition(0, 0);
        this._debugLabelNode.removeAllChildren(true);
        for (var i = 0; i < this._inNodeVertextArray.length; i ++) {
            this._debugDrawNode.drawDot(this._inNodeVertextArray[i], 5, cc.color.RED);
            var vertexLabel = new cc.LabelTTF(i + 1, MENU_FONT_NAME, 14);
            vertexLabel.setAnchorPoint(0.5, 0);
            vertexLabel.setColor(cc.color.RED);
            vertexLabel.setPosition(this._inNodeVertextArray[i].x, this._inNodeVertextArray[i].y + 10);
            this._debugLabelNode.addChild(vertexLabel);
        }
    },
    getVertexArray:function() {
        if (cc.sys.isNative) {
            /*
                web 情况下，cc.Action 在改变 cc.Node 的属性的情况下，会触发 JS 的同名 set 函数
                因此这里的 _transformedVertexArray 已经是变化过的结果，可以直接使用

                native 的情况下，cc.Action 在改变 cc.Node 的属性的情况下，不会触发 JS 的同名 set 函数
                因此这里有可能已发生了形变，可能需要更新
            */
            if (!cc.pointEqualToPoint(this.__oldPosition, this.getPosition())) {
                this._updateVertexArray();
                this._updateInNodeVertexArray();
                this._updateTransformedVertexArray();
                this.__oldPosition = cc.p(this.getPosition());
            }
            else {
                this._updateTransformedVertexArray();
            }
        }
        var result = this._transformedVertexArray.slice();    
        return result;
    },
    getRawVertexArray:function() {
        var result = this._vertexArray.slice();    
        return result;
    },
    removeVertex:function(index) {
        if (this._isPolygon && this._vertexArray.length <= 3) {
            cc.error("polygon can not have less than 3 vertexes");
            return;
        }
        else if (!this._isPolygon && this._vertexArray.length <= 2) {
            cc.error("polygon can not have less than 2 vertexes");
            return;
        }
        this._super();
    },
    delegateValidateTouchInNode:function(touchLocation) {
        return ssr.PolyHelper.isPointInPoly(touchLocation, this._vertexArray);
    }
});
