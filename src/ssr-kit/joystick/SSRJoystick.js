/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.Joystick = cc.Node.extend({
	ctor:function(backgroundFileName, backgroundPressedFileName, thumbFileName, thumbPressedFileName) {
		this._super();
		//
		this._stickPosition = cc.p(0, 0);
		this._angleInDegrees = 0;
		this._velocityUnitVector = cc.p(0, 0);
		this._enabled = true;
		this._rePositionRect = cc.rect(0, 0, 0, 0);
		//
		this._init(backgroundFileName, backgroundPressedFileName, thumbFileName, thumbPressedFileName);
		this._initTouchEvents();
	},
	_init:function(backgroundFileName, backgroundPressedFileName, thumbFileName, thumbPressedFileName) {
		this._initSprites(backgroundFileName, backgroundPressedFileName, thumbFileName, thumbPressedFileName);
		this.setThumbRadius(this._thumbSprite.getBoundingBox().width / 2);
		this.setJoystickRadius(this._backgroundSprite.getBoundingBox().width / 2);
	},
	_initSprites:function(backgroundFileName, backgroundPressedFileName, thumbFileName, thumbPressedFileName) {
		this._backgroundSprite = new cc.Sprite(backgroundFileName);
		this.addChild(this._backgroundSprite);
		
		this._backgroundPressedSprite = new cc.Sprite(backgroundPressedFileName);
		this._backgroundPressedSprite.setVisible(false);
		this.addChild(this._backgroundPressedSprite);
		
		this._thumbSprite = new cc.Sprite(thumbFileName);
		this.addChild(this._thumbSprite);
		
		this._thumbPressedSprite = new cc.Sprite(thumbPressedFileName);
		this._thumbPressedSprite.setVisible(false);
		this.addChild(this._thumbPressedSprite);
	},
	_initTouchEvents:function() {
		var self = this;
		this.touchListener = cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function(touch, event) {
				if (!self._enabled) {
					return false;
				}
				var location = touch.getLocation();
				if (cc.rectContainsPoint(self._rePositionRect, location)) {
                    self.setPosition(location);
                }
				location = self.convertToNodeSpace(location);
				if (self._processTouchBegan(touch)) {
					self.setOpacity(255);
	            	return true;
	        	};
	        	return false;
			},
            onTouchMoved: function(touch, event) {
				self._processTouchMoved(touch);
			},
            onTouchEnded: function(touch, event) {
				self._processTouchEnded(touch);
			},
            onTouchCancelled: function(touch, event) {
				self._processTouchEnded(touch);
			},
        }, this);
	},
	_moveStick:function() {
		this._thumbSprite.setPosition(this._stickPosition);
        this._thumbPressedSprite.setPosition(this._stickPosition);
	},
	_update:function(location) {
		var dx = location.x;
		var dy = location.y;
		var dSq = dx * dx + dy * dy;

		if(dSq <= 0) {
			this._velocityUnitVector = cc.p(0, 0);
			this._angleInDegrees = 0;
			this._stickPosition = location;
			this._moveStick();
			return;
		}

		var angle = Math.atan2(dy, dx);
		if(angle < 0) {
			angle += ssr.Joystick.SJ_PI_X_2;
		}
		var cosAngle = Math.cos(angle);
		var sinAngle = Math.sin(angle);
	
		if (dSq > this.joystickRadiusSq) {
			dx = cosAngle * this.joystickRadius;
			dy = sinAngle * this.joystickRadius;
		}
	
		this._velocityUnitVector = cc.p(dx / this.joystickRadius, dy / this.joystickRadius);
		this._angleInDegrees = angle * ssr.Joystick.SJ_RAD2DEG;
		this._stickPosition = cc.p(dx, dy);
		this._moveStick();
	},
	enable:function() {
		this._enabled = true;
	},
	disable:function() {
		this._enabled = false;
	},
	setRePositionRect:function(rect) {
		this._rePositionRect = rect;
	},
	isTriggered:function() {
		return (this._velocityUnitVector.x != 0 || this._velocityUnitVector.y != 0);
	},
	setOpacity:function(opacity) {
		this._super(opacity);
		this._backgroundSprite.setOpacity(opacity);
		this._backgroundPressedSprite.setOpacity(opacity);
		this._thumbSprite.setOpacity(opacity);
		this._thumbPressedSprite.setOpacity(opacity);
    },
	setJoystickRadius:function(r) {
		this.joystickRadius = r;
		this.joystickRadiusSq = r * r;
	},
	setThumbRadius:function(r) {
		this.thumbRadius = r;
		this.thumbRadiusSq = r * r;	
	},
	getVelocityUnitVector:function() {
        return this._velocityUnitVector;
    },
    getAngleInDegrees:function() {
        return this._angleInDegrees;
    },
	_processTouchBegan:function(touch) {
    	var location = touch.getLocation();
		location = this.convertToNodeSpace(location);
    	this._backgroundSprite.setVisible(false);
    	this._backgroundPressedSprite.setVisible(true);
    	this._thumbSprite.setVisible(false);
        this._thumbPressedSprite.setVisible(true);
    	var dSq = location.x * location.x + location.y * location.y;
    	if(this.joystickRadiusSq > dSq) {
        	this._update(location);
        	return true;
    	}
    },
	_processTouchMoved:function(touch) {
    	var location = touch.getLocation();
		location = this.convertToNodeSpace(location);
		this._update(location);
	},
	_processTouchEnded:function(touch) {
    	var location = cc.p(0, 0);
		this._update(location);
    	this._backgroundSprite.setVisible(true);
    	this._backgroundPressedSprite.setVisible(false);
    	this._thumbSprite.setVisible(false);
        this._thumbPressedSprite.setVisible(false);
        //
        this.setOpacity(100);
	},
	onExit:function() {
		cc.eventManager.removeListener(this.touchListener);
		this._super();
	}
});

ssr.Joystick.SJ_PI = 3.14159265359;
ssr.Joystick.SJ_PI_X_2 = 6.28318530718;
ssr.Joystick.SJ_RAD2DEG = 180.0 / ssr.Joystick.SJ_PI;
