/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.ToggleMenuPanelItem = cc.Node.extend({
    ctor:function(textArray, callback, delegate) {
        this._super();
        //
        this._textArray = textArray;
        this._callback = callback;
        this._delegate = delegate;

        this._menuItem = new cc.MenuItemToggle(new cc.MenuItemFont(this._textArray[0]), this._callback, this._delegate);
        for (var i = 1; i < this._textArray.length; i ++) {
            this._menuItem.addSubItem(new cc.MenuItemFont(this._textArray[i]));
        }
        this_menu = new cc.Menu(this._menuItem);
        this_menu.setPosition(0, 0);
        this.addChild(this_menu);

        this.setContentSize(
            this._menuItem.getContentSize().width,
            this._menuItem.getContentSize().height
        );
    },
    getValidTouchRect:function() {
        return cc.rect(
            -this.getContentSize().width * 0.5, 
            -this.getContentSize().height * 0.5, 
            this.getContentSize().width, 
            this.getContentSize().height
        );
    },
    touchFilter:function(touchLocation) {
        touchLocation = this.convertToNodeSpace(touchLocation);
        var validTouchRect = this.getValidTouchRect();
        if (validTouchRect && cc.rectContainsPoint(validTouchRect, touchLocation)) {
            return true;
        }
        else {
            return false;   
        }
    },
    canTouch:function(touchLocation) {
        return this.touchFilter(touchLocation);
    },
    getMenuItem:function() {
        return this._menuItem;
    }
});
