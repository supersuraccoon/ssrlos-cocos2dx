/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.ScrollPanel = ssr.Panel.extend({
    ctor:function(color, width, height) {
        this._super(color, width, height);
        // 
        this._items = [];
        this._alignment = ssr.ScrollPanel.ITEM_ALIGNMENT_CENTER;
        this._scrollContainer = new cc.LayerColor(color);
        this._scrollContainer.setContentSize(width * 0.8, height * 5);
        this._scrollView = new cc.ScrollView(cc.size(width * 0.8, height * 0.8), this._scrollContainer);
        this._scrollView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        this._scrollView.setTouchEnabled(true);
        this._scrollView.setContentOffset(
            cc.p(0, this._scrollView.getViewSize().height - this._scrollView.getContentSize().height)
        );
        this._scrollView.setPosition(
            width * 0.5 - this._scrollView.getViewSize().width * 0.5,
            height * 0.5 - this._scrollView.getViewSize().height * 0.5
        );
        this.addChild(this._scrollView);
    },
    // override
    touchFilter:function(touchLocation) {
        var isTouchInNode = cc.rectContainsPoint(this.getValidTouchRect(), touchLocation);
        var isTouchInScrollView = cc.rectContainsPoint(this._scrollView.getBoundingBox(), this.convertToNodeSpace(touchLocation));
        if (isTouchInNode && !isTouchInScrollView) {
            return true;
        }
        else {
            return false;   
        }
    },
    setItemAlignment:function(alignment) {
        this._alignment = alignment;
    },
    addItem:function(scrollPanelItem) {
        this._scrollContainer.addChild(scrollPanelItem);
        if (this._alignment == ssr.ScrollPanel.ITEM_ALIGNMENT_CENTER) {
            scrollPanelItem.setPosition(
                this._scrollContainer.getContentSize().width * 0.5,
                this._calcY(scrollPanelItem)
            );
    
        }
        else if (this._alignment == ssr.ScrollPanel.ITEM_ALIGNMENT_LEFT) {
            scrollPanelItem.setPosition(
                scrollPanelItem.getContentSize().width / 2,
                this._calcY(scrollPanelItem)
            );
        }
        this._items.push(scrollPanelItem)
    },
    _updateItems:function() {
        for (var i = 0; i < this._items.length; i ++) {
            if (this._alignment == ssr.ScrollPanel.ITEM_ALIGNMENT_CENTER) {
                this._items[i].setPosition(
                    this._scrollContainer.getContentSize().width * 0.5,
                    this._calcY(this._items[i])
                );
        
            }
            else if (this._alignment == ssr.ScrollPanel.ITEM_ALIGNMENT_LEFT) {
                this._items[i].setPosition(
                    this._scrollContainer.getContentSize().width * 0.0 + this._items[i].getContentSize().width / 2,
                    this._calcY(this._items[i])
                );
            }
        }
    },
    getItems:function() {
        return this._items;
    },
    addItems:function() {
        for (var i = 0; i < arguments.length; i++) {
            this.addItem(arguments[i]);
        }
    },
    _calcY:function(scrollPanelItem) {
        if (this._items.length > 0) {
            var preScrollPanelItem = this._items[this._items.length - 1];
            return preScrollPanelItem.getPositionY() - preScrollPanelItem.getContentSize().height / 2 - scrollPanelItem.getContentSize().height;
        }
        else {
            return this._scrollContainer.getContentSize().height - scrollPanelItem.getContentSize().height;
        }
    }
});

ssr.ScrollPanel.ITEM_ALIGNMENT_LEFT = 1;
ssr.ScrollPanel.ITEM_ALIGNMENT_CENTER = 2;
ssr.ScrollPanel.ITEM_ALIGNMENT_RIGHT = 3;
