/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.Panel = cc.LayerColor.extend({
    ctor:function(color, width, height) {
        this._super(color, width, height);
        this.ignoreAnchorPointForPosition(false);
        this._rightClickTriggered = false;
        this._dragTriggered = false;
        this._dragPrePosition = null;
        this._isHidden = false;
        this._autoHideThreshold = 40;
        //
        this._eventListerner = cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded,
            onTouchCancelled: this.onTouchCancelled,
        }, this);
        //
        if('mouse' in cc.sys.capabilities ) {
            var that = this;
            cc.eventManager.addListener({
                event: cc.EventListener.MOUSE,
                onMouseDown: this.onMouseDown
            }, this);
        }
        //
        var drawNode = new cc.DrawNode();
        drawNode.drawRect(
            cc.p(this.x, this.y),
            cc.p(this.x + this.width, this.y + this.height),
            cc.color(0, 0, 0, 200), 
            2,
            cc.color(255, 255, 255, 255)
        );
        this.addChild(drawNode);
    },
    enableTouch:function() {
        if (this._eventListerner) {
            return;
        }
        this._eventListerner = cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded,
            onTouchCancelled: this.onTouchCancelled,
        }, this);
    },
    disableTouch:function() {
        if (!this._eventListerner) {
            return;
        }
        cc.eventManager.removeListener(this._eventListerner);
        this._eventListerner = null;
    },
    getValidTouchRect:function() {
        return this.getBoundingBox();
    },
    dockLeft:function() {
        this.setPositionX(this.getContentSize().width * 0.5);
        this._isHidden = false;
    },
    hideLeft:function() {
        this.setPositionX(-this.getContentSize().width * 0.5 + this._autoHideThreshold);
        this._isHidden = true;
    },
    dockRight:function() {
        this.setPositionX(cc.winSize.width - this.getContentSize().width * 0.5);
        this._isHidden = false;
    },
    hideRight:function() {
        this.setPositionX(cc.winSize.width + this.getContentSize().width * 0.5 - this._autoHideThreshold);
        this._isHidden = true;
    },
    dockTop:function() {
        this.setPositionY(cc.winSize.height - this.getContentSize().height * 0.5);
        this._isHidden = false;
    },
    hideTop:function() {
        this.setPositionY(cc.winSize.height + this.getContentSize().height * 0.5 - this._autoHideThreshold);
        this._isHidden = true;
    },
    touchFilter:function(touchLocation) {
        var validTouchRect = this.getValidTouchRect();
        if (validTouchRect && cc.rectContainsPoint(validTouchRect, touchLocation)) {
            return true;
        }
        else {
            return false;   
        }
    },
    onTouchBegan:function (touch, event) {
        var target = event.getCurrentTarget();
        var touchLocation = touch.getLocation();
        if (target.touchFilter(touchLocation)) {
            if (!target._isHidden) {
                target._dragPrePosition = cc.p(touchLocation.x, touchLocation.y);
                target._dragTriggered = true;
            }
            return true;
        }
        else {
            return false;   
        }
    },
    onTouchMoved:function (touch, event) {
        var target = event.getCurrentTarget();
        if (target._rightClickTriggered) {
            return;
        }
        if (!target._dragTriggered) {
            return;
        }
        if (target._isHidden) {
            return;
        }
        // move
        var touchLocation = touch.getLocation();
        target.setPosition(
            cc.pAdd(target.getPosition(), cc.pSub(touchLocation, target._dragPrePosition))
        );
        target._dragPrePosition = touchLocation;
    },
    onTouchEnded:function (touch, event) {
        var target = event.getCurrentTarget();
        if (target._rightClickTriggered) {
            target._rightClickTriggered = false;
            return;
        }
        if (target._dragTriggered) {
            target._dragTriggered = false;
        }
        if (target._isHidden) {
            // dock
            if (target.getPositionX() < target.getContentSize().width * 0.5) {
                target.dockLeft();
            }
            if (target.getPositionX() > cc.winSize.width - target.getContentSize().width * 0.5) {
                target.dockRight();
            }
            if (target.getPositionY() > cc.winSize.height - target.getContentSize().height * 0.5) {
                target.dockRight();
            }
        }
        else {
            // hide
            if (target.getPositionX() < target.getContentSize().width * 0.5) {
                target.hideLeft();
            }
            if (target.getPositionX() > cc.winSize.width - target.getContentSize().width * 0.5) {
                target.hideRight();
            }

            if (target.getPositionY() > cc.winSize.height) {
                target.hideTop();
            }
        }
    },
    onTouchCancelled:function (touch, event) {
        var target = event.getCurrentTarget();
        if (target._rightClickTriggered) {
            target._rightClickTriggered = false;
            return;
        }
        if (target._dragTriggered) {
            target._dragTriggered = false;
        }
    },
    onMouseDown:function (event) {
        var target = event.getCurrentTarget();
        if (event._button == 2) {
            target._rightClickTriggered = true;
        }
    },
});
