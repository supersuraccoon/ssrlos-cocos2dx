/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.TextPanelItem = cc.Node.extend({
    ctor:function(text, fontName, fontSize, fontColor) {
        this._super();
        //
        this._text = text;
        this._label = new cc.LabelTTF(this._text, fontName, fontSize);
        this._label.setColor(fontColor || cc.color.WHITE);
        this.addChild(this._label);

        this.setContentSize(
            this._label.getContentSize().width,
            this._label.getContentSize().height
        );
    },
    getValidTouchRect:function() {
        return cc.rect(0, 0, 0, 0);
    },
    touchFilter:function(touchLocation) {
        touchLocation = this.convertToNodeSpace(touchLocation);
        var validTouchRect = this.getValidTouchRect();
        if (validTouchRect && cc.rectContainsPoint(validTouchRect, touchLocation)) {
            return true;
        }
        else {
            return false;   
        }
    },
    canTouch:function(touchLocation) {
        return this.touchFilter(touchLocation);
    }
});
