/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
/**
 * @classdesc The component for rendering sight area with mask effect (using cc.ClippingNode).
    JSBinding Support [○]
    JSBinding Functions Support:
        addTarget
        removeTarget
        removeTargets
        removeAllTargets
        updateTarget
        updateTargets
 * @class
 * @extends cc.Node
 * @prop {cc.LayerColor}                       maskLayer                        - A color layer for mask effect.
 * @prop {cc.Node}                             maskStencilNode                  - Mask stencilNode container.
 * @prop {cc.ClippingNode}                     maskClipper                      - The cc.ClippingNode instance.
 * @prop {Array.<cc.Node>}                     targets                          - The targets whoses sight area will be rendered for mask effect.
 * @prop {Array.<losComponentCoreProvider>}    losComponentCoreProviders        - Functions for providing ssr.LoS.Component.Core instance for each target node.
 * @prop {Array.<cc.DrawNode>}                 maskRenders                      - The stencil render, one per target node.
 */
ssr.LoS.Component.Mask = cc.Node.extend( /** @lends ssr.LoS.Component.Mask# */ {
    /**
     * The constructor
     * @function
     */
    ctor:function(maskColor) {
        this._super();
        if(!cc.sys.isNative && cc._renderType === cc.game.RENDER_TYPE_CANVAS) {
            cc.assert(0, "ssr.LoS.Component.Mask can only be used in WEBGL mode!")
            return;
        }
        //
        this._maskLayer = new cc.LayerColor(maskColor); 
        this._maskStencilNode = new cc.Node();
        this._maskClipper = new cc.ClippingNode();
        this._maskClipper.setInverted(true);
        this._maskClipper.stencil = this._maskStencilNode;
        this._maskClipper.addChild(this._maskLayer);
        this.addChild(this._maskClipper);
        //
        this._targets = [];
        this._losComponentCoreProviders = [];
        this._maskRenders = [];
    },
    /**
     * Add a target whose sight area will be rendered for mask effect.
     * @function
     * @param {cc.Node} node The target to be added.
     * @param {losComponentCoreProvider} losComponentCoreProvider Function which return the losComponentCore instance of a cc.Node.
     */
    addTarget:function(node, losComponentCoreProvider) {
        if(!node instanceof cc.Node) {
            cc.assert(0, "ssr.LoS.Component.Mask addTarget node need to be instance of cc.Node!")
            return;
        }
        if(!losComponentCoreProvider) {
            cc.assert(0, "ssr.LoS.Component.Mask addTarget losComponentCoreProvider is needed!")
            return;
        }
        var result = this.findTarget(node);
        if (result != -1) {
            cc.log("ssr.LoS.Component.Mask addTarget node already added");
        }
        else {
            this._targets.push(node);
            this._losComponentCoreProviders.push(losComponentCoreProvider);
            //
            var maskRender = new cc.DrawNode();
            this._maskStencilNode.addChild(maskRender);
            this._maskRenders.push(maskRender);
        }
    },
    /**
     * Remove a target.
     * @function
     * @param {cc.Node} node The target to be removed.
     * @return {Boolean} True for removed false for target not found.
     */
    removeTarget:function(node) {
        if(!node instanceof cc.Node) {
            cc.assert(0, "ssr.LoS.Component.Mask removeTarget node need to be instance of cc.Node!")
            return;
        }
        for (var i = 0, l = this._targets.length; i < l; i ++) {
            if (node === this._targets[i]) {
                this._targets.splice(i, 1);
                this._losComponentCoreProviders.splice(i, 1);
                this._maskRenders[i].removeFromParent(true);
                this._maskRenders.splice(i, 1);
                return true;
            }
        } 
        return false;
    },
    /**
     * Remove all the given targets.
     * @function
     * @param {Array.<cc.Node>} nodes The targets to be removed.
     */
    removeTargets:function(nodes) {
        for (var i = 0, l = nodes.length; i < l; i ++) {
            this.removeTarget(nodes[i]);
        } 
    },
    /**
     * Remove all the targets.
     * @function
     */
    removeAllTargets:function() {
        for (var i = 0, l = this._targets.length; i < l; i ++) {
            this.removeTarget(this._targets[i]);
        }
        this._targets = [];
        this._losComponentCoreProviders = [];
        this._maskRenders = [];
    },
    /**
     * Find if the given target is already added.
     * @function
     * @param {cc.Node} node The target to be removed.
     * @return {Number} Index of the node. -1 for not found
     */
    findTarget:function(node) {
        if(!node instanceof cc.Node) {
            cc.assert(0, "ssr.LoS.Component.Mask findTarget node need to be instance of cc.Node!")
            return -1;
        }
        for (var i = 0, l = this._targets.length; i < l; i ++) {
            if (node === this._targets[i]) {
                return i;
            }
        }
        return -1;
    },
    getRender:function(node) {
        var nodeIndex = this.findTarget(node);
        if (nodeIndex == -1) {
            cc.log("ssr.LoS.Component.Mask updateTarget node is not added!")
            return null;
        }
        return this._maskRenders[nodeIndex];
    },
    /**
     * Update the target's sight area mask effect.
     * @function
     * @param {cc.Node} node The target whose sight area to be updated.
     * @param {Boolean} [needForceUpdate=false] If need to update the target's sight area no matter it is changed or not.
     */
    updateTarget:function(node, needForceUpdate) {
        var nodeIndex = this.findTarget(node);
        if (nodeIndex == -1) {
            cc.log("ssr.LoS.Component.Mask updateTarget node is not added!")
        }
        var render = this._maskRenders[nodeIndex];
        var losComponentCoreProvider = this._losComponentCoreProviders[nodeIndex];
        needForceUpdate = (needForceUpdate === undefined ? false : needForceUpdate);
        var losComponentCore = losComponentCoreProvider.call(node);
        if (losComponentCore.isUpdated() || needForceUpdate) {
            render.clear();
            var sightAreaArray = losComponentCore.getSightArea();
            var sourcePosition = losComponentCore.getPosition();
            var screenPosition = losComponentCore.getOwnerScreenPosition();
            var deltaPosition = cc.pSub(screenPosition, sourcePosition);
            for (var i = 0, l = sightAreaArray.length; i < l; i ++) {
                var toDrawPoly = sightAreaArray[i].slice();
                if (losComponentCore.getMode() == ssr.LoS.Constant.MODE.UNLIMITED_RANGE) {
                    toDrawPoly.push(sourcePosition);
                }
                else if (losComponentCore.getMode() == ssr.LoS.Constant.MODE.LIMITED_RANGE_WITH_FULL_ANGLE) {
                    if (l > 1) {
                        toDrawPoly.push(sourcePosition);
                    }
                }
                else {
                    toDrawPoly.push(sourcePosition);
                }
                for (var j = 0, ll = toDrawPoly.length; j < ll; j ++) {
                    toDrawPoly[j] = cc.pAdd(toDrawPoly[j], deltaPosition);
                }
                render.drawPoly(toDrawPoly, ssr.LoS.Constant.RENDER_TRANSPARENT, ssr.LoS.Constant.RENDER_NO_BORDER, ssr.LoS.Constant.RENDER_TRANSPARENT);
            }
            //
            var obstacles = losComponentCore.getObstacles();
            for (var i = 0; i < obstacles.length; i ++) {
                if (obstacles[i].isSelfCulling()) {
                    render.drawPoly(obstacles[i].getVertexArray(), ssr.LoS.Constant.RENDER_TRANSPARENT, ssr.LoS.Constant.RENDER_NO_BORDER, ssr.LoS.Constant.RENDER_TRANSPARENT);
                }
            }
        }
    },
    /**
     * Update the targets' sight area mask effect.
     * @function
     * @param {Array.<cc.Node>} nodes The targets whose sight area to be updated.
     * @param {Boolean} [needForceUpdate=false] If need to update the target's sight area no matter it is changed or not.
     */
    updateTargets:function(nodes, needForceUpdate) {
        if (!nodes) {
            nodes = this._targets;
        }
        for (var i = 0, l = nodes.length; i < l; i ++) {
            this.updateTarget(nodes[i], needForceUpdate);
        }
    }
});

/**
 * The functoin will be called when trying to updating the vertex array of the ostacle.
 * @callback losComponentCoreProvider
 * @return {ssr.LoS.Component.Core} 
 */
