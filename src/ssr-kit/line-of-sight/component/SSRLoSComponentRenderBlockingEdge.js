/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
/**
 * @classdesc The component for rendering BlockingEdges.
    JSBinding Support [○]
    JSBinding Functions Support:
        enable
        disable
        setup
        plot
 * @class
 * @extends ssr.LoS.Component.RenderBase
 * @prop {cc.DrawNode}              render                           - The render instance.
 */
ssr.LoS.Component.RenderBlockingEdge = ssr.LoS.Component.RenderBase.extend( /** @lends ssr.LoS.Component.RenderBlockingEdge# */ {
    /**
     * The constructor
     * @function
     */
    ctor:function (losComponentCore) {
        this._super(losComponentCore);
    },
    /**
     * Init render instance.
     * @function
     * @private
     * @abstract
     */
    _initRender:function() {
        this._super();
        this._render = new cc.DrawNode();
        this.addChild(this._render);
        this.disable();
    },
    /**
     * Plot the BlockingEdges.
     * @function
     * @abstract
     */
    plot:function() {
        if (!this._canPlot()) {
            return;
        }
        this._super();
        ssr.LoS.Render.Util.renderBlockingEdge(
            this._losComponentCore, 
            this._render, 
            this._lineWidth, 
            this._lineColor, 
            this._isIgnoreSourcePosition
        );
    }
});
