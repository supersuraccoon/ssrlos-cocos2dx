/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
/**
 * @classdesc The component for rendering SightArea with soft light effect.
    JSBinding Support [○]
    JSBinding Functions Support:
        enable
        disable
        setup
        plot
 * @class
 * @extends ssr.LoS.Component.RenderBase
 * @prop {ssr.LoS.Render.Light}              render                     - The render instance.
 */
ssr.LoS.Component.RenderSightLight = ssr.LoS.Component.RenderBase.extend( /** @lends ssr.LoS.Component.RenderSightLight# */ {
    /**
     * The constructor
     * @function
     */
    ctor:function (losComponentCore) {
        this._super(losComponentCore);
    },
    /**
     * Init render instance.
     * @function
     * @private
     * @abstract
     */
    _initRender:function() {
        this._super();
        this._render = new ssr.LoS.Render.Light();
        this.addChild(this._render);
        this._isEnabled = false;
    },
    /**
     * Plot the SightArea with soft light effect.
     * @function
     * @abstract
     */
    plot:function() {
        if (!this._canPlot()) {
            return;
        }
        if (this._losComponentCore.getMode() == ssr.LoS.Constant.MODE.UNLIMITED_RANGE) {
            var rect = this._losComponentCore.getSightRect();
            this._render.setRadius(Math.sqrt(rect.width * rect.width, rect.height * rect.height));
            this._render.setSourceRadius(10);
        }
        else {
            // this._render.setRadius(this._losComponentCore.getRadius() * 1.2);
            // this._render.setSourceRadius(this._losComponentCore.getRadius() * 0.05);
        }
        this._super();
        this._render.setPosition(0, 0);
        this._render.setCenter(this._losComponentCore.getOwnerScreenPosition());
        ssr.LoS.Render.Util.renderSightArea(
            this._losComponentCore, 
            this._render, 
            this._fillColor, 
            ssr.LoS.Constant.RENDER_NO_BORDER, 
            ssr.LoS.Constant.RENDER_TRANSPARENT,
            this._isIgnoreSourcePosition
        );
    },
    getOwnerScreenPosition:function() {
        return this.getOwner().getParent() ? this.getOwner().getParent().convertToWorldSpace(this.getOwner().getPosition()) : this.getOwner().getPosition();
    },
    setRadius:function(radius) {
        this._render.setRadius(radius);
    },
    setSourceRadius:function(sourceRadius) {
        this._render.setSourceRadius(sourceRadius);
    },
    setIntensity:function(intensity) {
        this._render.setIntensity(intensity);
    },
    getRadius:function() {
        return this._render.getRadius();
    },
    getSourceRadius:function() {
        return this._render.getSourceRadius();
    },
    getIntensity:function() {
        return this._render.getIntensity();
    }
});
