/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
/**
 * @classdesc The base component for rendering different stuffs.
    JSBinding Support [○]
    JSBinding Functions Support:
        enable
        disable
        setup
        plot
 * @class
 * @extends cc.Node
 * @prop {ssr.LoS.Component.Core}      losComponentCore                 - The component core instance.
 * @prop {cc.Color}                    fillColor                        - Fill color for rendering.
 * @prop {cc.Color}                    lineColor                        - Line color for rendering.
 * @prop {Number}                      lineWidth                        - Line width for rendering.
 * @prop {Boolean}                     [isChildOfSource=true]           - If this component the child of the light source.
 * @prop {Boolean}                     [isIgnoreSourcePosition=true]    - If source position is ignored.
 * @prop {Boolean}                     [isIgnoreSourceRotation=true]    - If source rotation is ignored.
 * @prop {Boolean}                     [isIgnoreSourceScale=true]       - If source scale is ignored.
 */
ssr.LoS.Component.RenderBase = cc.Node.extend( /** @lends ssr.LoS.Component.RenderBase# */ {
    /**
     * The constructor
     * @function
     */
    ctor:function (losComponentCore) {
        this._super();
        this._losComponentCore = losComponentCore;
        this._fillColor = ssr.LoS.Constant.RENDER_TRANSPARENT;
        this._lineWidth = ssr.LoS.Constant.RENDER_NO_BORDER;
        this._lineColor = ssr.LoS.Constant.RENDER_TRANSPARENT;
        this._isChildOfSource = true;
        this._isIgnoreSourcePosition = true;
        this._isIgnoreSourceRotation = true;
        this._isIgnoreSourceScale = true;
        this._preRotation = cc.FLT_EPSILON;
        this._preScale = cc.FLT_EPSILON;
        this._initRender();
    },
    /**
     * Init render instance, need to be override
     * @function
     * @abstract
     * @private
     */
    _initRender:function() {
    },
    /**
     * Setup for the render instance.
     * @function
     */
    setup:function(fillColor, lineWidth, lineColor, isChildOfSource) {
        this.setFillColor(fillColor);
        this.setLineWidth(lineWidth);
        this.setLineColor(lineColor);
        this.setIsChildOfSource(isChildOfSource);
    },
    /**
     * Get the fill color of the render.
     * @function
     * @param {cc.Color} The fill color.
     */
    getFillColor:function() {
        return this._fillColor;
    },
    /**
     * Set the fill color of the render.
     * @function
     * @return {cc.Color} The fill color.
     */
    setFillColor:function(fillColor) {
        this._fillColor = fillColor;
    },
    /**
     * Get the line width of the render.
     * @function
     * @param {Number} The line width.
     */
    getLineWidth:function() {
        return this._lineWidth;
    },
    /**
     * Set the line width of the render.
     * @function
     * @return {Number} The line width.
     */
    setLineWidth:function(lineWidth) {
        this._lineWidth = lineWidth;
    },
    /**
     * Get the line color of the render.
     * @function
     * @param {Number} The line color.
     */
    getLineColor:function() {
        return this._lineColor;
    },
    /**
     * Set the line color of the render.
     * @function
     * @return {cc.Color} The line color.
     */
    setLineColor:function(lineColor) {
        this._lineColor = lineColor;
    },
    /**
     * Get the if the render will need to ignore the source position.
     * @function
     * @param {Boolean} If the render will need to ignore the source position.
     */
    getIsIgnoreSourcePosition:function() {
        return this._isIgnoreSourcePosition;
    },
    /**
     * Get render object.
     * @function
     * @param {cc.DrawNode} If the render will need to ignore the source position.
     */
    getRender:function() {
        return this._render;
    },
    /**
     * Set the if the render will need to ignore the source position.
     * @function
     * @return {Boolean} If the render will need to ignore the source position.
     */
    setIsIgnoreSourcePosition:function(isIgnoreSourcePosition) {
        this._isIgnoreSourcePosition = isIgnoreSourcePosition;
    },
    /**
     * Get the if the render will need to ignore the source position.
     * @function
     * @param {Boolean} If the render will need to ignore the source position.
     */
    getIsChildOfSource:function() {
        return this._isChildOfSource;
    },
    /**
     * Set the if the render will need to ignore the source position.
     * @function
     * @return {Boolean} If the render will need to ignore the source position.
     */
    setIsChildOfSource:function(isChildOfSource) {
        this._isChildOfSource = isChildOfSource;
        if (this._isChildOfSource) {
            this._isIgnoreSourcePosition = true;
            this._isIgnoreSourceRotation = true;
            this._isIgnoreSourceScale = true;
        }
        else {
            this._isIgnoreSourcePosition = false;
            this._isIgnoreSourceRotation = false;
            this._isIgnoreSourceScale = false;
        }
    },
    /**
     * Enable the render.
     * @function
     */
    isEnable:function() {
        return this._isEnabled;
    },
    /**
     * Enable the render.
     * @function
     */
    enable:function() {
        this._isEnabled = true;
        this._render.setVisible(true);
    },
    /**
     * Disable the render.
     * @function
     */
    disable:function() {
        this._isEnabled = false;
        this._render.setVisible(false);
    },
    /**
     * Check if can plot or not.
     * @function
     */
    _canPlot:function() {
        if (!this._losComponentCore || !this._isEnabled || !this._render) {
            return false;
        }
        else {
            return true;
        }
    },
    /**
     * Plot the elements.
     * @function
     * @abstract
     */
    plot:function() { 
        if (this._isIgnoreSourceRotation) {
            var parentRotation = this.getParent().getRotation();
            if (this._preRotation != parentRotation) {
                this._preRotation = parentRotation;
                this.setRotation(-parentRotation);
            }
        }
        if (this._isIgnoreSourceScale) {
            var parentScale = this.getParent().getScale();
            if (this._preScale != parentScale) {
                this._preScale = parentScale;
                this.setScale(1 / parentScale);
            }
        }
        this._render.clear();
    }
});
