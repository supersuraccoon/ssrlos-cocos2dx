/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
/**
 * Render util class
 * @class
 */
ssr.LoS.Render.Util = function() {};

/**
 * Render the visible area.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {cc.Color} fillColor Filled color.
 * @param {Number} lineWidth Border width.
 * @param {cc.Color} lineColor Border color.
 * @param {Boolean} [isIgnoreSourcePosition=false] Is the source position need to be ignored.
 */
ssr.LoS.Render.Util.renderSightArea = function(losComponentCore, render, fillColor, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var sightAreaArray = losComponentCore.getSightArea();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = sightAreaArray.length; i < l; i ++) {
        var toDrawPoly = sightAreaArray[i].slice();
        if (losComponentCore.getMode() == ssr.LoS.Constant.MODE.UNLIMITED_RANGE) {
            toDrawPoly.push(sourcePosition);
        }
        else if (losComponentCore.getMode() == ssr.LoS.Constant.MODE.LIMITED_RANGE_WITH_FULL_ANGLE) {
            if (l > 1) {
                toDrawPoly.push(sourcePosition);
            }
        }
        else {
            toDrawPoly.push(sourcePosition);
        }
        if (isIgnoreSourcePosition) {
            for (var j = 0, ll = toDrawPoly.length; j < ll; j ++) {
                toDrawPoly[j] = cc.pSub(toDrawPoly[j], sourcePosition);
            }
        }
        render.drawPoly(toDrawPoly, fillColor, lineWidth, lineColor);
    }
};

/**
 * Render all the verts.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {Number} lineWidth The size of the vert.
 * @param {cc.Color} lineColor The color of the vert.
 */
ssr.LoS.Render.Util.renderSightVert = function(losComponentCore, render, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var sightAreaArray = losComponentCore.getSightArea();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = sightAreaArray.length; i < l; i ++) {
        for (var j = 0; j < sightAreaArray[i].length; j ++) {
            if (isIgnoreSourcePosition) {
                render.drawDot(cc.pSub(sightAreaArray[i][j], sourcePosition), lineWidth, lineColor);
            }
            else {
                render.drawDot(sightAreaArray[i][j], lineWidth, lineColor);
            }
        }
    }
};
/**
 * Render all the blocking edges.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {Number} lineWidth The width of the edge.
 * @param {cc.Color} lineColor The color of the edge.
 */
ssr.LoS.Render.Util.renderBlockingEdge = function(losComponentCore, render, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var blockingEdgeArray = losComponentCore.getBlockingEdgeArray();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = blockingEdgeArray.length; i < l; i ++) {
        if (isIgnoreSourcePosition) {
            render.drawSegment(
                cc.pSub(blockingEdgeArray[i].getStartPoint(), sourcePosition), 
                cc.pSub(blockingEdgeArray[i].getEndPoint(), sourcePosition), 
                lineWidth, 
                lineColor
            );
        }
        else {
            render.drawSegment(
                blockingEdgeArray[i].getStartPoint(), 
                blockingEdgeArray[i].getEndPoint(), 
                lineWidth, 
                lineColor
            );
        }
    }
};
/**
 * Render all the potential blocking edges.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {Number} lineWidth The width of the edge.
 * @param {cc.Color} lineColor The color of the edge.
 */
ssr.LoS.Render.Util.renderPotentialBlockingEdge = function(losComponentCore, render, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var potentialBlockingEdgeArray = losComponentCore.getPotentialBlockingEdgeArray();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = potentialBlockingEdgeArray.length; i < l; i ++) {
        if (isIgnoreSourcePosition) {
            render.drawSegment(
                cc.pSub(potentialBlockingEdgeArray[i].getStartPoint(), sourcePosition), 
                cc.pSub(potentialBlockingEdgeArray[i].getEndPoint(), sourcePosition), 
                lineWidth, 
                lineColor
            );
        }
        else {
            render.drawSegment(
                potentialBlockingEdgeArray[i].getStartPoint(), 
                potentialBlockingEdgeArray[i].getEndPoint(), 
                lineWidth, 
                lineColor
            );
        }
    }
};
/**
 * Render all the visible edges.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {Number} lineWidth The width of the edge.
 * @param {cc.Color} lineColor The color of the edge.
 * @param {Boolean} [isIgnoreSourcePosition=false] Is the source position need to be ignored.
 */
ssr.LoS.Render.Util.renderVisibleEdge = function(losComponentCore, render, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var visibleEdgeArray = losComponentCore.getVisibleEdgeArray();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = visibleEdgeArray.length; i < l; i ++) {
        if (isIgnoreSourcePosition) {
            render.drawSegment(
                cc.pSub(visibleEdgeArray[i][0], sourcePosition), 
                cc.pSub(visibleEdgeArray[i][1], sourcePosition), 
                lineWidth, 
                lineColor
            );
        }
        else {
            render.drawSegment(
                visibleEdgeArray[i][0], 
                visibleEdgeArray[i][1],
                lineWidth, 
                lineColor
            );
        }
    }
};
/**
 * Render all the hit points.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {Number} lineWidth The size of the point.
 * @param {cc.Color} lineColor The color of the point.
 */
ssr.LoS.Render.Util.renderHitPoint = function(losComponentCore, render, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var hitPointArray = losComponentCore.getHitPointArray();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = hitPointArray.length; i < l; i ++) {
        if (isIgnoreSourcePosition) {
            render.drawDot(cc.pSub(hitPointArray[i].getHitPoint(), sourcePosition), lineWidth, lineColor);
        }
        else {
            render.drawDot(hitPointArray[i].getHitPoint(), lineWidth, lineColor);
        }
    }
};
/**
 * Render all the rays.
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {Number} lineWidth The width of the ray.
 * @param {cc.Color} lineColor The color of the ray.
 * @param {Boolean} [isIgnoreSourcePosition=false] Is the source position need to be ignored.
 */
ssr.LoS.Render.Util.renderRay = function(losComponentCore, render, lineWidth, lineColor, isIgnoreSourcePosition) {
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var hitPointArray = losComponentCore.getHitPointArray();
    var sourcePosition = losComponentCore.getPosition();
    for (var i = 0, l = hitPointArray.length; i < l; i ++) {
        if (isIgnoreSourcePosition) {
            render.drawSegment(
                cc.p(0, 0), 
                cc.pSub(hitPointArray[i].getHitPoint(), sourcePosition),
                lineWidth, 
                lineColor
            );
        }
        else {
            render.drawSegment(
                sourcePosition, 
                hitPointArray[i].getHitPoint(), 
                lineWidth, 
                lineColor
            );
        }
    }
};

/**
 * Render the circle of the sight range (except UNLIMITED_RANGE).
 * @function
 * @param {ssr.LoS.Component.Core} losComponentCore The ssr.LoS.Component.Core.
 * @param {cc.DrawNode} render The target to render in.
 * @param {cc.Color} fillColor Filled color.
 */
ssr.LoS.Render.Util.renderSightRange = function(losComponentCore, render, fillColor, isIgnoreSourcePosition) {
    if (losComponentCore.getMode() == ssr.LoS.Constant.MODE.UNLIMITED_RANGE) {
        return;
    }
    isIgnoreSourcePosition = isIgnoreSourcePosition == undefined ? false : isIgnoreSourcePosition;
    var sourcePosition = losComponentCore.getPosition();
    if (isIgnoreSourcePosition) {
        render.drawDot(cc.p(0, 0), losComponentCore.getRadius(), fillColor);
    }
    else {
        render.drawDot(sourcePosition, losComponentCore.getRadius(), fillColor);
    }
};
