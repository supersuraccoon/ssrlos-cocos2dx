/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/
 
/**
 * @classdesc The extension for cc.DrawNode to apply radial gradient effect to the rendering contents.
 * @class
 * @extends ssr.LoS.Render.Shader
 * @prop {Number}                              radius                           - Light radius.
 * @prop {Number}                              sourceRadius                     - Light source radius.
 * @prop {Number}                              fadeRadius                       - Light fade radius.
 * @prop {Number}                              fadeRadiusRatio                  - Light fade radius ratio.
 */
ssr.LoS.Render.Light = cc.DrawNode.extend( /** @lends ssr.LoS.Render.Light# */{
    /**
     * The constructor
     * @function
     */ 
    ctor:function(radius, sourceRadius, intensity) {
        this._radius = radius || 0; 
        this._sourceRadius = 0;
        this._intensity = intensity || 0.8; 
        this._worldPosition = cc.p(0, 0);
        this._super();
        this.applyShader();
        // 注意顺序，必须在 applyShader 后调用，否则无效
        this.setBlendFunc(new cc.BlendFunc(cc.ONE, cc.ONE_MINUS_SRC_COLOR));
    },
    setCenter:function(center) {
        if (!cc.pSameAs(center, this._worldPosition)) {
            this._worldPosition = center;
            this.updateUniforms();
        }
    },
    /**
     * Set the radius of the light.
     * @function
     * @param {Number} radius
     */
    setRadius:function(radius) {
        this._radius = radius;
        this.updateUniforms();
    },
    getRadius:function() {
        return this._radius;
    },
    /**
     * Set the source radius of the light.
     * @function
     * @param {Number} radius
     */
    setSourceRadius:function(radius) {
        this._sourceRadius = radius;
        this.updateUniforms();
    },
    getSourceRadius:function() {
        return this._sourceRadius;
    },
    /**
     * Set the fade radius ratio of the light.
     * @function
     * @param {Number} intensity
     */
    setIntensity:function(intensity) {
        this._intensity = intensity;
        this.updateUniforms();
    },
    getIntensity:function() {
        return this._intensity;
    },
    /**
     * Apply custom shader to the render.
     * @function
     * @private
     * @abstract
     */
    applyShader:function() {
        this._shaderProgram = new cc.GLProgram();
        this._shaderProgram.initWithString(ssr.LoS.Render.Light.SHADER_VERT, ssr.LoS.Render.Light.SHADER_FRAG);
        this._shaderProgram.addAttribute(cc.ATTRIBUTE_NAME_POSITION, cc.VERTEX_ATTRIB_POSITION);
        this._shaderProgram.addAttribute(cc.ATTRIBUTE_NAME_TEX_COORD, cc.VERTEX_ATTRIB_TEX_COORDS);
        this._shaderProgram.addAttribute(cc.ATTRIBUTE_NAME_COLOR, cc.VERTEX_ATTRIB_COLOR);
        this._shaderProgram.link();
        this._shaderProgram.updateUniforms();
        this.setShaderProgram(this._shaderProgram);
        this.updateUniforms();
    },
    /**
     * Update uniforms for the shader.
     * @function
     * @abstract
     */
    updateUniforms:function() {
        this._shaderProgram.use();
        //
        var frameSize = cc.view.getFrameSize();
        var visibleSize = cc.view.getVisibleSize();
        var retinaFactor = cc.view.getDevicePixelRatio();
        var x = this._worldPosition.x * frameSize.width / visibleSize.width * retinaFactor;
        var y = this._worldPosition.y * frameSize.height / visibleSize.height * retinaFactor;
        //
        if (cc.sys.isNative) {
            this.getGLProgramState().setUniformFloat(
                "radius",
                parseFloat(this._radius)
            );
            this.getGLProgramState().setUniformFloat(
                "sourceRadius",
                parseFloat(this._sourceRadius)
            );
            this.getGLProgramState().setUniformFloat(
                "intensity",
                parseFloat(this._intensity)
            );
            this.getGLProgramState().setUniformVec2(
                "center",
                cc.p(x, y)
            );
        }
        else {
            this._shaderProgram.setUniformLocationWith1f(
                "radius",
                this._radius
            );
            this._shaderProgram.setUniformLocationWith1f(
                "sourceRadius",
                this._sourceRadius
            );
            this._shaderProgram.setUniformLocationWith1f(
                "intensity",
                this._intensity
            );
            this._shaderProgram.setUniformLocationF32(
                "center",
                x, y
            );
        }
    }
});

/**
 * The vert shader for Render Light.
 * @constant
 * @type String
 */
ssr.LoS.Render.Light.SHADER_VERT =
        "#ifdef GL_ES\n"
        + "   attribute mediump vec4 a_position;\n"
        + "   attribute mediump vec2 a_texcoord;\n"
        + "   attribute mediump vec4 a_color;\n"
        + "   varying mediump vec4 v_color;\n"
        + "   varying mediump vec2 v_texcoord;\n"
        + "#else\n"
        + "   attribute vec4 a_position;\n"
        + "   attribute vec2 a_texcoord;\n"
        + "   attribute vec4 a_color;\n"
        + "   varying vec4 v_color;\n"
        + "   varying vec2 v_texcoord;\n"
        + "#endif\n"
        + "void main()\n"
        + "{\n"
        + "    v_color = vec4(a_color.rgb * a_color.a, a_color.a);\n"
        + "    v_texcoord = a_texcoord;\n"
        + "    gl_Position = CC_PMatrix * CC_MVMatrix * a_position;\n"
        + "}\n";

/**
 * The frag shader for Render Light.
 * @constant
 * @type String
 */
ssr.LoS.Render.Light.SHADER_FRAG =
        "#ifdef GL_ES \n"
        + "    varying lowp vec4 v_color;\n"
        + "    varying lowp vec2 v_texcoord;\n"
        + "    precision highp float;\n"
        + "#else\n"
        + "    varying vec4 v_color;\n"
        + "    varying vec2 v_texcoord;\n"
        + "#endif\n"
        + "uniform vec2 center; \n"
        + "uniform float radius; \n"
        + "uniform float sourceRadius; \n"
        + "uniform float intensity; \n"
        + "float circleDist(vec2 p, float radius) \n"
        + "{ \n"
        + " return length(p) - radius; \n"
        + "} \n"
        + "float fillMask(float dist) \n"
        + "{ \n"
        + " return clamp(-dist, 0.0, 1.0); \n"
        + "} \n"
        + "float shadow(vec2 p, vec2 pos, float radius) \n"
        + "{ \n"
        + " vec2 dir = normalize(pos - p); \n"
        + " float dl = length(p - pos); \n"
        + " float lf = radius * dl; \n"
        + " float dt = 0.01; \n"
        + " lf = clamp((lf*dl + radius) / (2.0 * radius), 0.0, 1.0); \n"
        + " lf = smoothstep(0.0, 1.0, lf); \n"
        + " return lf; \n"
        + "} \n"
        + "vec4 drawLight(vec2 p, vec2 pos, vec4 color, float range, float radius) \n"
        + "{ \n"
        + " float ld = length(p - pos); \n"
        + " if (ld > range) return vec4(0.0); \n"
        + " float shad = shadow(p, pos, radius); \n"
        + " float fall = (range - ld)/range; \n"
        + " fall *= fall; \n"
        + " float source = fillMask(circleDist(p - pos, radius)); \n"
        + " return (shad * fall + source) * color; \n"
        + "} \n"
        + "float luminance(vec4 col) \n"
        + "{ \n"
        + " return 0.2126 * col.r + 0.7152 * col.g + 0.0722 * col.b; \n"
        + "} \n"
        + "void setLuminance(inout vec4 col, float lum) \n"
        + "{ \n"
        + " lum /= luminance(col); \n"
        + " col *= lum; \n"
        + "} \n"
        + " float rand(vec2 co) {                                                    \n"
        + "     return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);   \n"
        + " }                                                                        \n"
        + " vec3 saturate(vec3 a){return clamp(a,0.,1.);} \n"
        + "void main() { \n"
        + "    vec2 p = gl_FragCoord.xy; \n"
        + "     vec4 lightCol = v_color; \n"
        + "     setLuminance(lightCol, intensity); \n"
        + "     vec4 col = drawLight(p, center, lightCol, radius, sourceRadius); \n"
        + "     gl_FragColor = clamp(col, 0.0, 1.0); \n"
        + "}"

ssr.LoS.Render.Light.prototype._ctor = function() {
    ssr.LoS.Render.Light.prototype.init.call(this);
};
