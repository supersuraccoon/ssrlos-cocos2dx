/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.Addon = {};

//
ssr.addon = function(target, sources, forceOverwrite) {
    if (sources == null || sources == undefined) {
        cc.assert(false, "cc.addon source can not be NULL!!!");
        return;
    }
    forceOverwrite = (forceOverwrite === undefined ? false : forceOverwrite);
    for(var key in sources) {
        if (sources.hasOwnProperty(key)) {
            if (target[key]) {
                if (forceOverwrite) {
                    target[key] = sources[key];
                }
            }
            else {
                target[key] = sources[key];
            }
        }
    }
};


ssr.addoff = function(target, sources) {
    if (sources == null || sources == undefined) {
        cc.assert(false, "ssr.addoff source can not be NULL!!!");
        return;
    }
    for(var key in sources) {
        if (sources.hasOwnProperty(key)) {
            if (target[key]) {
                delete target[key];
            }
            else {
                cc.log("Warning: %s not found in prototype", key);
            }
        }
    }
};



/*
    cc.Addon.SomeAddon = {
        // variable
        _variableA: xxx,
        _variableB: xxx,
        _variableC: xxx,
        // init function
        initAddon:function() {},
        // functions
        funcA:function() {},
        funcB:function() {},
        funcC:function() {}
    };
*/
