/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.Addon.Touchable = {
    _lastPressedPosition: null,
    _longPressTimeCounter: 0,
    _longPressFlag: false,
    _isSelected: false,
    _recordSelected: false,
    // public
    enableTouch:function() {
        this._eventListerner = cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded,
            onTouchCancelled: this.onTouchCancelled,
        }, this);
    },
    isSelected:function() {
        return this._isSelected;
    },
    select:function() {
        this._isSelected = true;
        if (this.delegateTouchSelect) {
            this.delegateTouchSelect();
        }
    },
    deselect:function() {
        this._isSelected = false;
        if (this.delegateTouchDeselect) {
            this.delegateTouchDeselect();
        }
    },
    disableTouch:function() {
        if (this._eventListerner) {
            cc.eventManager.removeListener(this._eventListerner);
            this._eventListerner = null;
        }
    },
    // private
    _validateTouch:function(position) {
        var myRect = this.getBoundingBox();
        if (!myRect) {
            return false;
        }
        var pos = this.getPosition();
        myRect.x = pos.x - myRect.width / 2;
        myRect.y = pos.y - myRect.height / 2;
        return cc.rectContainsPoint(myRect, position);
    },
    onTouchBegan:function (touch, event) {
        var target = event.getCurrentTarget();
        var touchPoint = touch.getLocation();
        var isValidTouch = false;
        if (target.delegateValidateTouch) {
            isValidTouch = target.delegateValidateTouch(touchPoint);
        }
        else {
            isValidTouch = target._validateTouch(touchPoint);
        }
        if (!isValidTouch) {
            return false;
        }
        if (target.delegateTouchBegan) {
            target.delegateTouchBegan(touch, event);
        }
        target._lastPressedPosition = cc.p(touchPoint.x , touchPoint.y);
        target._recordSelected = true;
        target.scheduleOnce(target.longPressTriggerCallback, 1);
        return true;
    },
    onTouchMoved:function (touch, event) {
        var target = event.getCurrentTarget();
        var touchPoint = touch.getLocation();
        if (target._longPressFlag) {
            // do not move in long press mode
            return;
        }

        // long press mode
        if (target._lastPressedPosition) {
            var distance = cc.pDistance(target._lastPressedPosition, cc.p(touchPoint.x, touchPoint.y));
            if (distance > 1) {
                target.unschedule(target.longPressTriggerCallback);
                target._lastPressedPosition = null;
                target._longPressFlag = false;
                target._recordSelected = false;
            }
            else {
                target._lastPressedPosition.x = touchPoint.x;
                target._lastPressedPosition.y = touchPoint.y;
            }
        }
        // delegate
        if (target.delegateTouchMoved) {
            target.delegateTouchMoved(touch, event);
        }
    },
    onTouchEnded:function (touch, event) {
        var target = event.getCurrentTarget();
        var touchPoint = touch.getLocation();
        if (!target._longPressFlag) {
            
        }
        if (target.delegateTouchEnded) {
            target.delegateTouchEnded(touch, event);
        }
        if (!target._longPressFlag) {
            if (target._recordSelected) {
                if (target.isSelected()) {
                    target.deselect();
                }
                else {
                    target.select();
                }    
            }
        }
        target._recordSelected = false;
        target.unschedule(target.longPressTriggerCallback);
        target._lastPressedPosition = null;
        target._longPressFlag = false;
    },
    onTouchCancelled:function (touch, event) {
        var target = event.getCurrentTarget();
        target.unschedule(target.longPressTriggerCallback);
        target._lastPressedPosition = null;
        target._longPressFlag = false;
        target._recordSelected = false;
        if (target.delegateTouchCanceled) {
            target.delegateTouchCanceled();
        }
    },
    setTouchCallback:function(callbacks) {
        this.delegateTouchBegan = callbacks.delegateTouchBegan;
        this.delegateTouchMoved = callbacks.delegateTouchMoved;
        this.delegateTouchEnded = callbacks.delegateTouchEnded;
        this.delegateTouchCanceled = callbacks.delegateTouchCanceled;
        this.delegateTouchLongPressed = callbacks.delegateTouchLongPressed;
        this.delegateTouchSelect = callbacks.delegateTouchSelect;
        this.delegateTouchDeselect = callbacks.delegateTouchDeselect;
    },
    setValidateTouchCallback:function(callback) {
         this.delegateValidateTouch = callback;
    },
    clearValidateTouchCallback:function() {
         this.delegateValidateTouch = null;
    },
    longPressTriggerCallback:function() {
        this._longPressFlag = true;
        if (this.delegateTouchLongPressed) {
            this.delegateTouchLongPressed(this._lastPressedPosition);
        }
    },
    onExit:function() {
        this.disableTouch();
        this._super();
    }
};
