/****************************************************************************
 Copyright (c) 2017-2020 SuperSuRaccoon
 
 Site: http://www.supersuraccoon-cocos2d.com
 Mail: supersuraccoon@gmail.com

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var ssr = ssr || {};

ssr.KeyboardController = cc.Node.extend({
    ctor:function (delegate) {
        this._super();
        // keys
        this._allKeys = {};
        this._isEnabled = true;
        this._delegate = delegate;
        // event
        if('keyboard' in cc.sys.capabilities) {
            var self = this;
            this._keyboardEvent = cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed:  function(keyCode, event) {
                    if (!self._isEnabled) {
                        return;
                    }
                    self._allKeys[keyCode] = true;
                    if (self._delegate && self._delegate.onKeyPressed) {
                        self._delegate.onKeyPressed(keyCode, self);
                    }
                },
                onKeyReleased: function(keyCode, event) {
                    if (!self._isEnabled) {
                        return;
                    }
                    self._allKeys[keyCode] = false;
                    if (self._delegate && self._delegate.onKeyReleased) {
                        self._delegate.onKeyReleased(keyCode, self);
                    }
                }
            }, this);
        }
        else {
            cc.log("keyboard is not supported !!!");
        }
    },
    getPressedKeys:function() {
        var keys = [];
        for (var key in this._allKeys) {
            if (this._allKeys[key]) {
                keys.push(key);
            }
        }
        return keys;
    },
    getKeyName:function(keyCode) {
        for (keyName in cc.KEY) {
            if (cc.KEY[keyName] == keyCode) {
                return keyName;
            }
        }
        return "?" + keyCode + "?";
    },
    isKeyPressed:function(keyCode) {
        return this._allKeys[keyCode];
    },
    resetKeys:function() {
        this._allKeys = {};  
    },
    enable:function() {
        this._isEnabled = true;
    },
    disable:function() {
        this._isEnabled = false;
    },
    isEnable:function() {
        return this._isEnabled;
    },
    onEnter:function() {
        this._super();
    },
    onExit:function() {
        if (this._keyboardEvent) {
            cc.eventManager.removeListener(this._keyboardEvent);
        }
        this._delegate = null;
        this._super();
    }
});
