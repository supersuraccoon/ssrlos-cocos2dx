
var TestLimitedRangeWithReflexAngleLoS = TestLoSCullingBase.extend({
    ctor:function () {
        this._super();
    },
    _initTestCase:function() {
        this._testCaseNameArray = [
            //
            "BEHIND.BEHIND/0Edge.0Arc",
            "BEHIND.BEHIND/1Edge.0Arc",
            "BEHIND.BEHIND/1Edge.1Arc",
            "BEHIND.BEHIND/2Edges.0Arc",
            "BEHIND.BEHIND/0Edge.0Arc",
            "BEHIND.BEHIND/2Edges.2Arcs",
            //
            "OUT.OUT/0Edge.0Arc",
            "OUT.OUT/1Edge.1Arc",
            "OUT.OUT/0Edge.2Arcs",
            "OUT.OUT/0Edge.0Arc",
            //
            "IN.IN/0Edge.0Arc",
            "IN.IN/0Edge.0Arc",
            "IN.IN/2Edges.0Arc",
            //
            "BEHIND.OUT/0Edges.0Arc",
            "BEHIND.OUT/1Edge.1Arc",
            "BEHIND.OUT/1Edge.0Arc",
            "BEHIND.OUT/1Edge.1Arc",
            //
            "BEHIND.IN/1Edge.0Arc",
            "BEHIND.IN/1Edge.0Arc",
            //
            "OUT.IN/0Edge.1Arc",
            "OUT.IN/1Edge.0Arc",
            "OUT.IN/2Edges.1Arc"
        ];
        this._firstTestCase = "BEHIND.BEHIND/0Edge.0Arc";
    },
    processCullingTest:function(cullingTestCase) {
        // 删除所有的障碍物
        ObstacleObjectManager.getInstance().removeAllObstacles();
        // 清除所有障碍物
        this._robotObject.getLoSComponentCore().removeAllObstacles();
        //
        this._robotObject.setPosition(cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5));
        // 关闭自动生成边界
        this._robotObject.getLoSComponentCore().setRadius(200);
        //
        this._robotObject.getLoSComponentCore().setCentralAngle(270);
        //
        this._super(cullingTestCase);
    },
    _generatePolylines:function(cullingTestCase) {
        var testCasePolylines = [];
        //
        if (cullingTestCase.startsWith("BEHIND.BEHIND")) {
            if (cullingTestCase == "BEHIND.BEHIND/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 - 150, cc.winSize.height * 0.5 - 70)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.BEHIND/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.2.*: 一个端点在扇形某条边上
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 50)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.BEHIND/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.3.*: 一个端点在扇形某条边和圆弧交点上
                        cc.p(cc.winSize.width * 0.5 - 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.BEHIND/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.4.*: 两个端点在扇形不同的两边上
                        cc.p(cc.winSize.width * 0.5 - 100 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 100 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 - 100 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 - 100 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.BEHIND/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.5.*: 两个端点和扇形某边共线
                        cc.p(cc.winSize.width * 0.5 - 100 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 100 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 - 50 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 50 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.BEHIND/2Edges.2Arcs") {
                testCasePolylines = [
                    [
                        // case.6.*: 两个端点在扇形两条边和圆弧交点上
                        cc.p(cc.winSize.width * 0.5 - 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 - 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 - 200 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("OUT.OUT")) {
            if (cullingTestCase == "OUT.OUT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 - 150, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 - 220, cc.winSize.height * 0.5 - 250)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.OUT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/1弧
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 220),
                        cc.p(cc.winSize.width * 0.5 - 220, cc.winSize.height * 0.5 - 250)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.OUT/0Edge.2Arcs") {
                testCasePolylines = [
                    [
                        // case.3: 0边/2弧
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 + 120, cc.winSize.height * 0.5 - 250)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.OUT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.5.*: 边和扇形某边共线
                        cc.p(cc.winSize.width * 0.5 - 220, cc.winSize.height * 0.5 - 220),
                        cc.p(cc.winSize.width * 0.5 + 220, cc.winSize.height * 0.5 + 220)   
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("IN.IN")) {
            if (cullingTestCase == "IN.IN/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 50),
                        cc.p(cc.winSize.width * 0.5 + 120, cc.winSize.height * 0.5 - 90)
                    ]
                ];
            }
            else if (cullingTestCase == "IN.IN/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 120),
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 150)
                    ]
                ];
            }
            else if (cullingTestCase == "IN.IN/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.2: 2边/0弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 150),
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 - 150)
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("BEHIND.OUT")) {
            if (cullingTestCase == "BEHIND.OUT/0Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 - 250, cc.winSize.height * 0.5)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.OUT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/1弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 - 50),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 250)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.OUT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.3.*: 一点在扇形某边上
                        cc.p(cc.winSize.width * 0.5 - 250, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 - 100)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.OUT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.4.*: 边和扇形某边共线
                        cc.p(cc.winSize.width * 0.5 - 220, cc.winSize.height * 0.5 - 220),
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 50)
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("BEHIND.IN")) {
            if (cullingTestCase == "BEHIND.IN/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 1边/0弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 - 50),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 - 80)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.IN/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        //case.2.*: 1边在扇形的边上
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 50),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 - 80)
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("OUT.IN")) {
            if (cullingTestCase == "OUT.IN/0Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/1弧
                        cc.p(cc.winSize.width * 0.5 + 250, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 - 100)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.IN/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/0弧
                        cc.p(cc.winSize.width * 0.5 - 250, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 80)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.IN/2Edges.1Arc") {
                testCasePolylines = [
                    [
                        // case.3: 2边/1弧
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 150)
                    ]
                ];
            }
        }
        return testCasePolylines;
    }
});
