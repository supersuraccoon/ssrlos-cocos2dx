
var TestLimitedRangeWithNonReflexAngleLoS = TestLoSCullingBase.extend({
    ctor:function () {
        this._super();
    },
    _initTestCase:function() {
        this._testCaseNameArray = [
            "BEHIND.BEHIND",
            //
            "FRONT.FRONT/0Edge.0Arc",
            "FRONT.FRONT/2Edges.0Arc",
            "FRONT.FRONT/1Edge.0Arc",
            "FRONT.FRONT/2Edges.0Arc",       // x
            "FRONT.FRONT/2Edges.2Arcs",      // x
            "FRONT.FRONT/2Edges.1Arc",       // x
            "FRONT.FRONT/1Edge.0Arc",
            "FRONT.FRONT/2Edges.0Arc",       // x
            //
            "OUT.OUT/0Edge.0Arc",
            "OUT.OUT/1Edge.1Arc",
            "OUT.OUT/0Edge.2Arcs",
            "OUT.OUT/2Edges.0Arc",
            //
            "IN.IN/0Edge.0Arc",
            "IN.IN/0Edge.2Arcs",
            "IN.IN/0Edge.0Arc",              // x
            //
            "BEHIND.FRONT/0Edge.0Arc",
            "BEHIND.FRONT/2Edges.0Arc",
            "BEHIND.FRONT/2Edges.0Arc",
            "BEHIND.FRONT/1Edge.1Arc",
            "BEHIND.FRONT/1Edge.0Arc",
            "BEHIND.FRONT/1Edge.0Arc",
            //
            "BEHIND.OUT/0Edge.0Arc",
            "BEHIND.OUT/1Edge.1Arc",
            "BEHIND.OUT/2Edges.0Arc",
            "BEHIND.OUT/0Edge.0Arc",
            //
            "BEHIND.IN",
            //
            "OUT.IN/0Edge.1Arc",
            "OUT.IN/1Edge.0Arc",
            "OUT.IN/0Edge.2Arcs",
            //
            "OUT.FRONT/0Edge.0Arc",
            "OUT.FRONT/1Edge.0Arc",
            "OUT.FRONT/2Edges.0Arc",
            "OUT.FRONT/1Edge.1Arc",
            "OUT.FRONT/1Edge.1Arc",          // x
            //
            "IN.FRONT/1Edge.0Arc",
            "IN.FRONT/1Edge.0Arc",           // x
            "IN.FRONT/1Edge.1Arc"
        ];
        this._firstTestCase = "BEHIND.BEHIND";
    },
    processCullingTest:function(cullingTestCase) {
        // 删除所有的障碍物
        ObstacleObjectManager.getInstance().removeAllObstacles();
        // 清除所有障碍物
        this._robotObject.getLoSComponentCore().removeAllObstacles();
        //
        this._robotObject.setPosition(cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5));
        // 关闭自动生成边界
        this._robotObject.getLoSComponentCore().setRadius(200);
        //
        this._robotObject.getLoSComponentCore().setCentralAngle(90);
        //
        this._super(cullingTestCase);
    },
    _generatePolylines:function(cullingTestCase) {
        var testCasePolylines = [];
        if (cullingTestCase == "BEHIND.BEHIND") {
            // 添加测试用例
            testCasePolylines = [
                [
                    // case.1: 0边/0弧
                    cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5),
                    cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 50)
                ]
            ];
        }
        else if (cullingTestCase.startsWith("OUT.OUT")) {
            if (cullingTestCase == "OUT.OUT/0Edge.0Arc") {
                this._robotObject.getLoSComponentCore().setCentralAngle(30);
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 + 200),
                        cc.p(cc.winSize.width * 0.5 + 220, cc.winSize.height * 0.5 + 50)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.OUT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/1弧
                        cc.p(cc.winSize.width * 0.5 + 230, cc.winSize.height * 0.5 + 200),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 - 200)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.OUT/0Edge.2Arcs") {
                testCasePolylines = [
                    [
                        // case.3: 2弧
                        cc.p(cc.winSize.width * 0.5 + 180, cc.winSize.height * 0.5 + 200),
                        cc.p(cc.winSize.width * 0.5 + 180, cc.winSize.height * 0.5 - 200)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.OUT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.4: 2边
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 200),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 - 200)
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("FRONT.FRONT")) {
            if (cullingTestCase == "FRONT.FRONT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 + 10, cc.winSize.height * 0.5 + 50),
                        cc.p(cc.winSize.width * 0.5 + 20, cc.winSize.height * 0.5 + 80)
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.2: 2边/0弧
                        cc.p(cc.winSize.width * 0.5 + 60, cc.winSize.height * 0.5 + 100),
                        cc.p(cc.winSize.width * 0.5 + 60, cc.winSize.height * 0.5 - 100)
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.*3: 两点在扇形的同一边上  => segmentSegmentTest false
                        cc.p(cc.winSize.width * 0.5 + 10, cc.winSize.height * 0.5 + 10),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 + 80)
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.*4: 两点在扇形的不同一边上
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 + 80),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 - 80)
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/2Edges.2Arcs") {
                testCasePolylines = [
                    [
                        // case.*5: 两点在扇形边和圆弧的两个交点上 => segmentSegmentTest true
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 - 200 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/2Edges.1Arc") {
                testCasePolylines = [
                    [
                        // case.*6: 一点在扇形边上，一点在扇形边和圆弧的交点上
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 - 80)
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.*7: 一点在扇形边上，一点在FRONT，0边/0弧
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 + 80),
                        cc.p(cc.winSize.width * 0.5 + 10, cc.winSize.height * 0.5 + 100)
                    ]
                ];
            }
            else if (cullingTestCase == "FRONT.FRONT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.*8: 一点在扇形边上，一点在FRONT，2边/0弧
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 - 80),
                        cc.p(cc.winSize.width * 0.5 + 10, cc.winSize.height * 0.5 + 100)
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("IN.IN")) {
            if (cullingTestCase == "IN.IN/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 + 120, cc.winSize.height * 0.5 - 50),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 + 50)
                    ]
                ];
            }
            else if (cullingTestCase == "IN.IN/0Edge.2Arcs") {
                testCasePolylines = [
                    [
                        // case.*2: 两点都在圆弧上
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(20 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(20 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(20 * cc.RAD), cc.winSize.height * 0.5 - 200 * Math.sin(20 * cc.RAD))
                    ]
                ];
            }
            else if (cullingTestCase == "IN.IN/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.*2: 两点和圆心共线
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 + 150, cc.winSize.height * 0.5)
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("BEHIND.FRONT")) {
            if (cullingTestCase == "BEHIND.FRONT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 - 100),
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 + 150)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/0弧 case.4.*: 一点在扇形的某边上
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 50),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 100)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.FRONT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.3: 2边/0弧
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 180),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 150)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.FRONT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.5.*: 一点在扇形的某边上，共有两个交点
                        cc.p(cc.winSize.width * 0.5 - 80, cc.winSize.height * 0.5 + 150),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 - 100)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.FRONT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.6.*: 一点在扇形边和圆弧的交点上
                        cc.p(cc.winSize.width * 0.5 - 80, cc.winSize.height * 0.5 + 150),
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.7.*: 边和扇形的某边共线，测试结果 true/false，会被后续的条件判断剔除，没有问题
                        cc.p(cc.winSize.width * 0.5 - 100 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 - 100 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 100 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 100 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("BEHIND.OUT")) {
            if (cullingTestCase == "BEHIND.OUT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 150),
                        cc.p(cc.winSize.width * 0.5 + 300, cc.winSize.height * 0.5 + 150)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.OUT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/1弧
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 50),
                        cc.p(cc.winSize.width * 0.5 + 300, cc.winSize.height * 0.5)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.OUT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.3: 2边/0弧
                        cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 + 150),
                        cc.p(cc.winSize.width * 0.5 + 140, cc.winSize.height * 0.5 - 200)
                    ]
                ];
            }
            else if (cullingTestCase == "BEHIND.OUT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.4.*: 边和扇形的一边共线
                        cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 - 100),
                        cc.p(cc.winSize.width * 0.5 + 250, cc.winSize.height * 0.5 + 250)
                    ]
                ];
            }
        }
        else if (cullingTestCase == "BEHIND.IN") {
            // 添加测试用例
            testCasePolylines = [
                [
                    // case.1: 1边/0弧
                    cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 100),
                    cc.p(cc.winSize.width * 0.5 + 150, cc.winSize.height * 0.5 + 80)
                ]
            ];
        }
        else if (cullingTestCase.startsWith("OUT.IN")) {
            if (cullingTestCase == "OUT.IN/0Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/1弧
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 80),
                        cc.p(cc.winSize.width * 0.5 + 250, cc.winSize.height * 0.5)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.IN/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/0弧
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5),
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 + 250)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.IN/0Edge.2Arcs") {
                testCasePolylines = [
                    [
                        // case.3: 0边/2弧
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(20 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(20 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 150, cc.winSize.height * 0.5 - 250),
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("OUT.FRONT")) {
            if (cullingTestCase == "OUT.FRONT/0Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 0边/0弧
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 + 100)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.2: 1边/0弧 case.5.*: 一点在扇形的某边上
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 + 80)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.FRONT/2Edges.0Arc") {
                testCasePolylines = [
                    [
                        // case.3: 2边/0弧
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 + 80, cc.winSize.height * 0.5 - 150)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.FRONT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.4: 1边/1弧
                        cc.p(cc.winSize.width * 0.5 + 250, cc.winSize.height * 0.5 + 250),
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 - 150)
                    ]
                ];
            }
            else if (cullingTestCase == "OUT.FRONT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.6.*: 一点在扇形边和圆弧的交点上
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5 - 250),
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 200 * Math.sin(45 * cc.RAD))
                    ]
                ];
            }
        }
        else if (cullingTestCase.startsWith("IN.FRONT")) {
            if (cullingTestCase == "IN.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.1: 1边/0弧
                        cc.p(cc.winSize.width * 0.5 + 100, cc.winSize.height * 0.5 + 150),
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5)
                    ]
                ];
            }
            else if (cullingTestCase == "IN.FRONT/1Edge.0Arc") {
                testCasePolylines = [
                    [
                        // case.2.*: 一点在扇形的某边上
                        cc.p(cc.winSize.width * 0.5 + 100 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 + 100 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5)
                    ]
                ];
            }
            else if (cullingTestCase == "IN.FRONT/1Edge.1Arc") {
                testCasePolylines = [
                    [
                        // case.3.*: 一点在扇形边和圆弧的交点上
                        cc.p(cc.winSize.width * 0.5 + 200 * Math.cos(45 * cc.RAD), cc.winSize.height * 0.5 - 200 * Math.sin(45 * cc.RAD)),
                        cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.5)
                    ]
                ];
            }
        }
        return testCasePolylines;
    }
});
