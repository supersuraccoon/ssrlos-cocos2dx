
var TestUnlimitedRangeLoS = TestLoSCullingBase.extend({
    ctor:function () {
        this._super();
    },
    _initTestCase:function() {
        this._testCaseNameArray = [
            "IN.IN",
            "ON.ON",
            "OUT.OUT",
            "IN.ON",
            "IN.OUT",
            "ON.OUT"
        ];
        this._firstTestCase = "IN.IN";
    },
    processCullingTest:function(cullingTestCase) {
        // 删除所有的障碍物
        ObstacleObjectManager.getInstance().removeAllObstacles();
        // 清除所有障碍物
        this._robotObject.getLoSComponentCore().removeAllObstacles();
        //
        this._robotObject.setPosition(cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5));
        // 关闭自动生成边界
        // this._robotObject.getLoSComponentCore().disableAutoGenerateBoundary();
        //
        // 设置自定义边界
        // this._robotObject.getLoSComponentCore().setSightSize(
        //     cc.winSize.width * 0.5, cc.winSize.height * 0.5
        // );
        this._super(cullingTestCase);
    },
    
    _generatePolylines:function(cullingTestCase) {
        var testCasePolylines = [];
        if (cullingTestCase == "IN.IN") {
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.25 + 50, cc.winSize.height * 0.25 + 50),
                    cc.p(cc.winSize.width * 0.25 + 100, cc.winSize.height * 0.25 + 150)
                ]
            ];
        }
        else if (cullingTestCase == "ON.ON") {
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.25, cc.winSize.height * 0.25 + 50),
                    cc.p(cc.winSize.width * 0.25, cc.winSize.height * 0.25 + 150)
                ],
                [
                    cc.p(cc.winSize.width * 0.25 + 250, cc.winSize.height * 0.25),
                    cc.p(cc.winSize.width * 0.25 + 300, cc.winSize.height * 0.75)
                ]
            ];
        }
        else if (cullingTestCase == "OUT.OUT") {
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.25 - 50, cc.winSize.height * 0.25 + 50),
                    cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.25 - 100)
                ],
                [
                    cc.p(cc.winSize.width * 0.75, cc.winSize.height * 0.75 + 100),
                    cc.p(cc.winSize.width * 0.75, cc.winSize.height * 0.25 - 100)
                ],
                [
                    cc.p(cc.winSize.width * 0.25 - 50, cc.winSize.height * 0.75 - 50),
                    cc.p(cc.winSize.width * 0.25 + 50, cc.winSize.height * 0.75 + 50)
                ],
                [
                    cc.p(cc.winSize.width * 0.5 + 50, cc.winSize.height * 0.25 - 50),
                    cc.p(cc.winSize.width * 0.5 - 150, cc.winSize.height * 0.25 - 150)
                ]
            ];
        }
        else if (cullingTestCase == "IN.ON") {
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 50),
                    cc.p(cc.winSize.width * 0.75, cc.winSize.height * 0.25)
                ],
                [
                    cc.p(cc.winSize.width * 0.25 + 100, cc.winSize.height * 0.25 + 100),
                    cc.p(cc.winSize.width * 0.25, cc.winSize.height * 0.75)
                ]
            ];
        }
        else if (cullingTestCase == "IN.OUT") {
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.5 - 50),
                    cc.p(cc.winSize.width * 0.75 + 50, cc.winSize.height * 0.25 - 100)
                ],
                [
                    cc.p(cc.winSize.width * 0.25 + 100, cc.winSize.height * 0.25 + 100),
                    cc.p(cc.winSize.width * 0.25 - 50, cc.winSize.height * 0.25 - 50)
                ]
            ];
        }
        else if (cullingTestCase == "ON.OUT") {
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.25),
                    cc.p(cc.winSize.width * 0.5 + 150, cc.winSize.height * 0.25 - 100)
                ],
                [
                    cc.p(cc.winSize.width * 0.75, cc.winSize.height * 0.5 + 100),
                    cc.p(cc.winSize.width * 0.75 - 100, cc.winSize.height * 0.75 + 150)
                ]
            ];
        }
        return testCasePolylines;
    }
});
