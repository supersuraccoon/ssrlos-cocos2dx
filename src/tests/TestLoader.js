//
var TestLayer = cc.Layer.extend({
    ctor:function (testInfo) {
        this._super();
        // test info
        this.testInfo = testInfo;
        cc.MenuItemFont.setFontSize(cc.sys.isNative ? 30 : 22);
        cc.MenuItemFont.setFontName(MENU_FONT_NAME);

        if (this.testInfo.test == "TestUnlimitedRangeLoS") {
            this._test = new TestUnlimitedRangeLoS();
        }
        else if (this.testInfo.test == "TestLimitedRangeWithFullAngleLoS") {
            this._test = new TestLimitedRangeWithFullAngleLoS();
        }
        else if (this.testInfo.test == "TestLimitedRangeWithNonReflexAngleLoS") {
            this._test = new TestLimitedRangeWithNonReflexAngleLoS();
        }
        else if (this.testInfo.test == "TestLimitedRangeWithReflexAngleLoS") {
            this._test = new TestLimitedRangeWithReflexAngleLoS();
        }
        else if (this.testInfo.test == "TestLoSPerformance") {
            this._test = new TestLoSPerformance();
        }
        else if (this.testInfo.test == "TestLoSPlayground") {
            this._test = new TestLoSPlayground();
        }
        this.addChild(this._test);
        
        // title
        var demoTitle = new cc.LabelTTF(this.testInfo.title, MENU_FONT_NAME, cc.sys.isNative ? 30 : 20);
        demoTitle.setPosition(cc.winSize.width * 0.5, cc.winSize.height * 0.96);
        this.addChild(demoTitle, 1, "demoTitle");
        
        // menu
        var backButton = new cc.MenuItemFont("Back", this.backCallback, this);
        backButton.setAnchorPoint(0.5, 0);
        var menu = cc.Menu.create(backButton);
        menu.setPosition(cc.winSize.width * 0.5, cc.winSize.height * 0.01);
        this.addChild(menu, 1, "backButton");
    },
    backCallback:function() {
        cc.director.runScene(new DemoScene());
    }
});
var TestScene = cc.Scene.extend({
    ctor:function (testInfo) {
        this._super();
        var layer = new TestLayer(testInfo);
        this.addChild(layer);
    }
});
