
var TestLimitedRangeWithFullAngleLoS = TestLoSCullingBase.extend({
    ctor:function () {
        this._super();
    },
    _initTestCase:function() {
        this._testCaseNameArray = [
            "IN.IN",
            "ON.ON",
            "OUT.OUT",
            "IN.ON",
            "IN.OUT",
            "ON.OUT"
        ];
        this._firstTestCase = "IN.IN";
    },
    processCullingTest:function(cullingTestCase) {
        // 删除所有的障碍物
        ObstacleObjectManager.getInstance().removeAllObstacles();
        // 清除所有障碍物
        this._robotObject.getLoSComponentCore().removeAllObstacles();
        //
        this._robotObject.setPosition(cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5));
        // 关闭自动生成边界
        this._robotObject.getLoSComponentCore().setRadius(200);
        //
        this._super(cullingTestCase);
    },
    _generatePolylines:function(cullingTestCase) {
        var testCasePolylines = [];
        if (cullingTestCase == "IN.IN") {
            // 添加测试用例
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 150, cc.winSize.height * 0.5 + 100),
                    cc.p(cc.winSize.width * 0.5 - 50, cc.winSize.height * 0.25 + 50)
                ]
            ];
        }
        else if (cullingTestCase == "ON.ON") {
            // 添加测试用例
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 200, cc.winSize.height * 0.5),
                    cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5 - 200)
                ]
            ];
        }
        else if (cullingTestCase == "OUT.OUT") {
            // 添加测试用例
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.25 - 100, cc.winSize.height * 0.75 + 100),
                    cc.p(cc.winSize.width * 0.25 - 50, cc.winSize.height * 0.75 + 50)
                ],
                [
                    cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 300),
                    cc.p(cc.winSize.width * 0.5 + 300, cc.winSize.height * 0.5 - 100)
                ],
                [
                    cc.p(cc.winSize.width * 0.25 - 150, cc.winSize.height * 0.5 - 200),
                    cc.p(cc.winSize.width * 0.75 + 100, cc.winSize.height * 0.5 - 200)
                ]
            ];
        }
        else if (cullingTestCase == "IN.ON") {
            // 添加测试用例
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 100),
                    cc.p(cc.winSize.width * 0.5 + 200, cc.winSize.height * 0.5)
                ]
            ];
        }
        else if (cullingTestCase == "IN.OUT") {
            // 添加测试用例
            testCasePolylines = [
                [
                    cc.p(cc.winSize.width * 0.5 - 100, cc.winSize.height * 0.5 + 100),
                    cc.p(cc.winSize.width * 0.5 + 300, cc.winSize.height * 0.5 - 100)
                ]
            ];
        }
        else if (cullingTestCase == "ON.OUT") {
            // 添加测试用例
            testCasePolylines = [
                [
                    // 1交点
                    cc.p(cc.winSize.width * 0.5 - 200, cc.winSize.height * 0.5),
                    cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5 + 300)
                ],
                [   
                    // 向外
                    cc.p(cc.winSize.width * 0.5 + 200, cc.winSize.height * 0.5),
                    cc.p(cc.winSize.width * 0.5 + 300, cc.winSize.height * 0.5)
                ],
                [   
                    // 相切
                    cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.5 - 200),
                    cc.p(cc.winSize.width * 0.5 - 200, cc.winSize.height * 0.5 - 200)
                ]
            ];
        }
        return testCasePolylines;
    }
});
