
var TestLoSPerformance = TestLoSMapBase.extend({
    ctor:function () {
        this._super();
        //
        this._lightSources = [];
        this.randomObstacles(10);
    },
    _initFloatingMenu:function() {
        this._super();
        //
        this._performanceTextPanelItem = new ssr.TextPanelItem(
            "  --- Performance ---",
            cc.sys.isNative ? cc.MenuItemFont.getFontName(): cc.MenuItemFont.fontName(), 
            cc.sys.isNative ? cc.MenuItemFont.getFontSize(): cc.MenuItemFont.fontSize()
        );

        this._randomObstaclesMenuPanelItem = new ssr.ToggleMenuPanelItem(
            [
                "  Obstacles ✖ 10",
                "  Obstacles ✖ 20",
                "  Obstacles ✖ 30",
                "  Obstacles ✖ 40",
                "  Obstacles ✖ 50",
                "  Obstacles ✖ 60",
                "  Obstacles ✖ 70",
                "  Obstacles ✖ 80",
                "  Obstacles ✖ 90",
                "  Obstacles ✖ 100",
            ], this._randomObstaclesMenuCallback, this
        );
        this._randomLightsMenuPanelItem = new ssr.ToggleMenuPanelItem(
            [
                "  Lights ✖ 0",
                "  Lights ✖ 2",
                "  Lights ✖ 4",
                "  Lights ✖ 6",
                "  Lights ✖ 8",
                "  Lights ✖ 10",
            ], this._randomLightsMenuCallback, this
        );
        this._autoRotationMenuPanelItem = new ssr.ToggleMenuPanelItem(
            [
                "  Rotation Off",
                "  Rotation On" 
            ], this._autoRotationMenuCallback, this
        );

        this._floatingMenu.addItems(
            this._performanceTextPanelItem,
            this._randomObstaclesMenuPanelItem,
            this._randomLightsMenuPanelItem,
            this._autoRotationMenuPanelItem
            
        );
    },
    spawnOneObstacle:function(size) {
        var sides = 6;
        var vertArray = [];
        for (var i = 0; i < sides; i++) {
            vertArray.push(cc.p(
                size * Math.cos(i / sides * Math.PI * 2),
                size * Math.sin(i / sides * Math.PI * 2))
            );
        }
        return ObstacleObjectManager.getInstance().createObstacle(vertArray);
    },
    randomLights:function(count) {
        // generate
        var mapSize = this._mainLayer.getContentSize();

        for (var i = 0; i < this._lightSources.length; i ++) {
            this._lightSources[i].removeFromParent(true);
        }
        this._lightSources = [];

        var w = this._mainLayer.width;
        var h = this._mainLayer.height;
        var size = 10;
        var hCount = 10;

        for (var i = 0; i < count; i ++) {
            var light = new LightObject(150, 8, 0.5, cc.color(Math.random() * 255, Math.random() * 255, Math.random() * 255, 255));
            this._mainLayer.addChild(light);
            light.setPosition(10, size + (h - size) / hCount * i + 30);
            light.runAction(
                cc.RepeatForever.create(
                    cc.Sequence.create(
                        cc.MoveBy.create(20 + i * 2, cc.p(w - 10, 0)),
                        cc.MoveBy.create(20 + i * 2, cc.p(-w + 10, 0))
                    )
                )
            );
            this._lightSources.push(light);
            var obstacles = ObstacleObjectManager.getInstance().getObstacleArray();
            for (var o = 0; o < obstacles.length; o ++) {
                var obstacle = light.getLoSComponentCore().addObstacle(obstacles[o], obstacles[o].getVertexArray(), obstacles[o]._isPolygon);
                obstacle.setVertexArrayProvider(obstacles[o].getVertexArray);
            }    
        }
    },
    randomObstacles:function(count) {
        // clear first
        ObstacleObjectManager.getInstance().removeAllObstacles();
        this._robotObject.getLoSComponentCore().removeAllObstacles();

        // generate
        var mapSize = this._mainLayer.getContentSize();

        var w = this._mainLayer.width;
        var h = this._mainLayer.height;
        var size = (count > 100 ? 10 : 12);
        var wCount = count / 10;
        var hCount = 10;

        this._obstacles = [];
        for (var i = 0; i < wCount; i ++) {
            for (var j = 0; j < hCount; j ++) {
                var polyNode = this.spawnOneObstacle(size);
                polyNode.setPosition(size + w / (wCount + 1) * (i + 1), size + (h - size) / hCount * j);
                this._mainLayer.addChild(polyNode);
                this._obstacles.push(polyNode);
                var obstacle = this._robotObject.getLoSComponentCore().addObstacle(polyNode, polyNode.getVertexArray());
                this._robotObject.getLoSComponentCore().addObstacle(polyNode);
                obstacle.setVertexArrayProvider(polyNode.getVertexArray);
            }
        }

        var obstacles = ObstacleObjectManager.getInstance().getObstacleArray();
        for (var i = 0; i < this._lightSources.length; i ++) {
            this._lightSources[i].getLoSComponentCore().removeAllObstacles();
            for (var o = 0; o < obstacles.length; o ++) {
                var obstacle = this._lightSources[i].getLoSComponentCore().addObstacle(obstacles[o], obstacles[o].getVertexArray(), obstacles[o]._isPolygon);
                obstacle.setVertexArrayProvider(obstacles[o].getVertexArray);
            }
        }
    },
    _randomLightsMenuCallback:function(sender) {
        var randomoLightsCount = (sender.getSelectedIndex()) * 2;
        this.randomLights(randomoLightsCount);
    },
    _randomObstaclesMenuCallback:function(sender) {
        var randomoObstaclesCount = (sender.getSelectedIndex() + 1) * 10;
        this.randomObstacles(randomoObstaclesCount);
    },
    _autoRotationMenuCallback:function(sender) {
        if (sender.getSelectedIndex() == 0) {
            for (var i = 0; i < this._obstacles.length; i ++) {
                this._obstacles[i].stopAllActions();
            }
        }
        else {
            for (var i = 0; i < this._obstacles.length; i ++) {
                this._obstacles[i].runAction(cc.RepeatForever.create(cc.RotateBy.create(6.0, 360)));
            }
        }
    },
    update:function(dt) {
        this._super();
        // process lights
        for (var i = 0; i < this._lightSources.length; i ++) {
            this._lightSources[i].updateSightNode();
        }
    },
    enableDirtyDetection:function() {
        this._super();
        //
        for (var i = 0; i < this._lightSources.length; i ++) {
            var obstacles = this._lightSources[i].getLoSComponentCore().getObstacles();
            for (var o = 0; o < obstacles.length; o ++) {
                obstacles[o].enableDirtyDetection();
            }
        }
    },
    disableDirtyDetection:function() {
        this._super();
        //
        for (var i = 0; i < this._lightSources.length; i ++) {
            var obstacles = this._lightSources[i].getLoSComponentCore().getObstacles();
            for (var o = 0; o < obstacles.length; o ++) {
                obstacles[o].disableDirtyDetection();
            }
        }
    },
});
