//
var TestLoSCullingBase = cc.Layer.extend({
    ctor:function () {
        this._super();
        //
        this._initSubLabel();
        this._initTestCase();
        this._initFloatingMenu();
        this._initMainLayer();
        this._initRobot();
        this._initLoSComponentRender();
        this._initKeyboard();
        //
        this.processCullingTest(this._firstTestCase);
        this.scheduleUpdate();
    },
    _initSubLabel:function() {
        this._testCaseLabel = new cc.LabelTTF("", MENU_FONT_NAME, cc.sys.isNative ? 22 : 18);
        this._testCaseLabel.setPosition(cc.winSize.width * 0.5, cc.winSize.height * 0.9);
        this.addChild(this._testCaseLabel);
    },
    _initTestCase:function() {
        this._testCaseNameArray = [];
        this._firstTestCase = "";
        // need override
    },
    _initFloatingMenu:function() {
        this._floatingMenu = new ssr.ScrollPanel(cc.color(0, 0, 0, 150), cc.winSize.width * 0.25, cc.winSize.height * 0.8);
        this._floatingMenu.setPosition(cc.winSize.width * 0.5, cc.winSize.height * 0.5);
        this._floatingMenu.dockRight();
        
        cc.MenuItemFont.setFontSize(cc.sys.isNative ? 18 : 14);
        this._titleTextPanelItem = new ssr.TextPanelItem(
            "------ Test Case ------",
            cc.sys.isNative ? cc.MenuItemFont.getFontName(): cc.MenuItemFont.fontName(), 
            cc.sys.isNative ? cc.MenuItemFont.getFontSize(): cc.MenuItemFont.fontSize(),
            cc.color.WHITE
        );
        this._floatingMenu.addItem(
            this._titleTextPanelItem
        );

        this._testCaseMenuItemArray = [];
        for (var i = 0; i < this._testCaseNameArray.length; i ++) {
            this._testCaseMenuItemArray.push(
                new ssr.MenuPanelItem(
                    this._testCaseNameArray[i], 
                    this._cullingMenuCallback, 
                    this
                )
            );
            this._floatingMenu.addItem(
                this._testCaseMenuItemArray[i]
            );
        }
        this.addChild(this._floatingMenu, 999999);
    },
    _initMainLayer:function() {
        this._mainLayer = new cc.LayerColor(cc.color(0, 0, 0, 0), cc.winSize.width, cc.winSize.height);
        this.addChild(this._mainLayer, 1);
        //
        this._gridLine = new cc.Sprite(res.gridLine_png);
        this._gridLine.setPosition(this._mainLayer.width / 2, this._mainLayer.height / 2);
        this._gridLine.setOpacity(200);
        this._mainLayer.addChild(this._gridLine);
    },
    _initRobot:function() {
        this._robotObject = new RobotObject();
        this._mainLayer.addChild(this._robotObject, 2);
    },
    _initLoSComponentRender:function() {
        // ray
        this._losComponentRenderRay = new ssr.LoS.Component.RenderRay(this._robotObject.getLoSComponentCore());
        this._losComponentRenderRay.setup(ssr.LoS.Constant.RENDER_TRANSPARENT, 1, cc.color(255, 255, 0, 100));
        this.addChild(this._losComponentRenderRay);
        // hitPoint
        this._losComponentRenderHitPoint = new ssr.LoS.Component.RenderHitPoint(this._robotObject.getLoSComponentCore());
        this._losComponentRenderHitPoint.setup(ssr.LoS.Constant.RENDER_TRANSPARENT, 4, cc.color.GREEN);
        this.addChild(this._losComponentRenderHitPoint);
        // sightArea
        this._losComponentRenderSightArea = new ssr.LoS.Component.RenderSightArea(this._robotObject.getLoSComponentCore());
        this._losComponentRenderSightArea.setup(cc.color(255, 255, 0, 100), ssr.LoS.Constant.RENDER_NO_BORDER, ssr.LoS.Constant.RENDER_TRANSPARENT);
        this.addChild(this._losComponentRenderSightArea);
        //
        this._losComponentRenderRay.enable();
        this._losComponentRenderHitPoint.enable();
        this._losComponentRenderSightArea.enable();
    },
    _initKeyboard:function() {
        if(cc.sys.isNative) {
            this.joyStickLayer = new JoyStickLayer();
            this.addChild(this.joyStickLayer, 999999999);
        }
        else {
            this._keyboardController = new ssr.KeyboardController(this);
            this.addChild(this._keyboardController);
        }
    },
    _processRobotMoveAndRotate:function(dt) {
        var moveDirection = 0;
        var rotationDelta = 0;
        var velocityScale = cc.p(1, 1);
        if (this._keyboardController) {
            var targetRotation = this._robotObject.getRotation();
            if (this._keyboardController._allKeys[38] || this._keyboardController._allKeys[87]) {
                moveDirection = 1;
            }
            else if (this._keyboardController._allKeys[40] || this._keyboardController._allKeys[83]) {
                moveDirection = -1;
            }
            if (this._keyboardController._allKeys[37] || this._keyboardController._allKeys[65]) {
                rotationDelta = -2;
            }
            else if (this._keyboardController._allKeys[39] || this._keyboardController._allKeys[68]) {
                rotationDelta = 2;
            }
        }
        else {
            if (cc.sys.isNative && this.joyStickLayer && this.joyStickLayer.joystick.velocity.x != 0 || this.joyStickLayer.joystick.velocity.y != 0) {
                
                rotationDelta = 0;
                if (this.joyStickLayer.joystick.degrees != 0) {
                    this._robotObject.setRotation(-this.joyStickLayer.joystick.degrees);
                }
                moveDirection = 1;
                velocityScale = this.joyStickLayer.joystick.velocity;
            }
        }
        if (rotationDelta != 0) {
            this._robotObject.setRotation(targetRotation + rotationDelta);
        }
        if (moveDirection != 0) {
            var desiredPosition = cc.pAdd(
                this._robotObject.getPosition(), 
                this._robotObject.getDesiredDeltaPosition(moveDirection, velocityScale)
            );
            this._robotObject.setPosition(desiredPosition);
        }
    },
    _processRobotSight:function(dt) {
        var isUpdated = this._robotObject.updateSightNode();
        if (isUpdated) {
            this._losComponentRenderSightArea.plot();
            this._losComponentRenderRay.plot();
            this._losComponentRenderHitPoint.plot();
        }
    },
    update:function(dt) {
        this._processRobotMoveAndRotate(dt);
        this._processRobotSight(dt);
    },
    processCullingTest:function(cullingTestCase) {
        var testCasePolylines = this._generatePolylines(cullingTestCase);
        for (var i = 0; i < testCasePolylines.length; i ++) {
            var polygonlineNode = ObstacleObjectManager.getInstance().createObstacle(testCasePolylines[i], false);
            this._mainLayer.addChild(polygonlineNode);
            this._robotObject.getLoSComponentCore().addObstacle(polygonlineNode, polygonlineNode.getVertexArray(), false);
        }
    },
    _generatePolylines:function(cullingTestCase) {
        // need override
    },
    _cullingMenuCallback:function(sender, data) {
        this.processCullingTest(sender.getLabel().getString());
    },
});
