//
var RobotObject = cc.Node.extend({
    ctor:function() {
        this._super();
        this._sprite = new cc.Sprite("res/robot.png");
        this._sprite.setScale(0.45);
        this._velocity = cc.sys.isNative ? 3 : 2;
        this._preRotation = -999999;
        this.addChild(this._sprite);
        this._losComponentCore = this.addComponent(ssr.LoS.Component.Core, this);
    },
    setColor:function(color) {
        this._sprite.setColor(color);
    },
    getLoSComponentCore:function() {
        return this._losComponentCore;
    },
    updateSightNode:function(isForceLoSUpdate) {
        return (this._losComponentCore.update() || isForceLoSUpdate);
    },
    getDesiredDeltaPosition:function(moveDirection, velocityScale) {
        var desiredPosition = cc.p(0, 0);
        var verlocityUnit = cc.pForAngle(cc.degreesToRadians(-this.getRotation()));
        var velocity = cc.pMult(verlocityUnit, this._velocity);
        velocity.x *= Math.abs(velocityScale.x);
        velocity.y *= Math.abs(velocityScale.y);
        if (moveDirection == 1) {
            desiredPosition = velocity;
        }   
        else if (moveDirection == -1) {
            desiredPosition = cc.pMult(velocity, -1);
        }
        return desiredPosition;
    },
    onExit:function() {
        this._super();
    }
});
