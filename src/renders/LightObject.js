//
var LightObject = cc.Node.extend({
    ctor:function(radius, sourceRadius, intensity, color) {
        this._super();
        //
        this._needForceUpdate = false;
        this._lightColor = color;
        this.addLoSComponentCore(radius);

        // sightLight
        this._losComponentRenderSightLight = new ssr.LoS.Component.RenderSightLight(this._losComponentCore);
        this._losComponentRenderSightLight.setup(this._lightColor, ssr.LoS.Constant.RENDER_NO_BORDER, ssr.LoS.Constant.RENDER_TRANSPARENT, true);
        this.addChild(this._losComponentRenderSightLight);
        this._losComponentRenderSightLight.enable();
        //
        this._losComponentRenderSightLight.setRadius(radius * 1.2);
        this._losComponentRenderSightLight.setSourceRadius(sourceRadius);
        this._losComponentRenderSightLight.setIntensity(intensity);
    },
    getLoSComponentCore:function() {
        return this._losComponentCore;
    },
    addLoSComponentCore:function(radius) {
        this._losComponentCore = new ssr.LoS.Component.Core(this);
        this._losComponentCore.setRadius(radius);
        this._losComponentCore.enableCulling();
    },
    needForceUpdate:function() {
        return this._needForceUpdate;
    },
    resetNeedForceUpdate:function() {
        this._needForceUpdate = false;  
    },
    updateSightNode:function() {
        var isUpdated = this._losComponentCore.update();
        if (isUpdated || this._needForceUpdate) {
            this._losComponentRenderSightLight.plot();
        }
    },
    setRadius:function(radius) {
        this._losComponentRenderSightLight.setRadius(radius);
    },
    setSourceRadius:function(sourceRadius) {
        this._losComponentRenderSightLight.setSourceRadius(sourceRadius);
    },
    setIntensity:function(intensity) {
        this._losComponentRenderSightLight.setIntensity(intensity);
    },
    getRadius:function() {
        return this._losComponentRenderSightLight.getRadius();
    },
    getSourceRadius:function() {
        return this._losComponentRenderSightLight.getSourceRadius();
    },
    getIntensity:function() {
        return this._losComponentRenderSightLight.getIntensity();
    },
    onExit:function() {
        this._super();
    }
});
