//
var TESTS_ARRAY = [
    {
        "title"         : "Unlimited Range",
        "test"          : "TestUnlimitedRangeLoS"
    },
    {
        "title"         : "Limited Range - Full Angle",
        "test"          : "TestLimitedRangeWithFullAngleLoS"
    },
    {
        "title"         : "Limited Range - NonReflex",
        "test"          : "TestLimitedRangeWithNonReflexAngleLoS"
    },
    {
        "title"         : "Limited Range - Reflex",
        "test"          : "TestLimitedRangeWithReflexAngleLoS"
    },
    {
        "title"         : "Performance",
        "test"          : "TestLoSPerformance"
    },
    {
        "title"         : "Playground",
        "test"          : "TestLoSPlayground"
    }
];

var MENU_FONT_NAME = "Arial Black";
//
var DemoLayer = cc.Layer.extend({
    ctor:function () {
        this._super();
        //
        this.initTitle();
        this.initTestLists();
    },
    initTitle:function() {
        if (this.titleLabel) {
            return;
        }
        this.titleLabel = new cc.LabelTTF("SSRLoS-Cocos2dx", MENU_FONT_NAME, cc.sys.isNative ? 40 : 36);
        this.titleLabel.setPosition(cc.winSize.width * 0.5, cc.winSize.height * 0.92);
        this.addChild(this.titleLabel);
    },
    initTestLists:function() {
        // load tests
        this.testList = new cc.Layer(cc.winSize.width, cc.winSize.height * 0.85);
        this.testList.setPositionY(this.titleLabel.y - this.titleLabel.height * 2 - this.testList.height);
        this.addChild(this.testList);
        this._menus = [];

        var count = 0;
        for (var j = 0; j < TESTS_ARRAY.length; j ++) {
            var testInfo = TESTS_ARRAY[j];
            var testNameLabel = new cc.LabelTTF(testInfo.title, MENU_FONT_NAME, cc.sys.isNative ? 30 : 24);
            testNameLabel.setColor(cc.color(244, 246, 242));
            var testButton = new cc.MenuItemLabel(testNameLabel, function(sender) {
                cc.director.runScene(new TestScene(sender.testInfo));
            }, this);
            testButton.testInfo = testInfo;
            var menu = new cc.Menu(testButton);
            menu.setPosition(
                this.testList.getContentSize().width * 0.5, 
                this.testList.getContentSize().height * 0.98 - testButton.getContentSize().height * 2 * j
            );
            this.testList.addChild(menu);
            this._menus.push(menu);
        }
    },
});

var DemoScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new DemoLayer();
        layer.init();
        this.addChild(layer);
    }
});
