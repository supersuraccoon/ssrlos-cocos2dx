// JoyStickLayer.js
var JoyStickLayer = cc.Layer.extend({
    ctor:function () {
        this._super();
        //
        cc.spriteFrameCache.addSpriteFrames(res.joystick_plist);
        this.joystick = new ssr.Joystick(
            "#joystick_normal.png",
            "#joystick_pressed.png",
            "#joystick_thumb.png",
            "#joystick_thumb.png"
        );
        this.joystick.setPosition(cc.winSize.width - 150, cc.winSize.height * 0.2);
        this.joystick.setRePositionRect(cc.rect(cc.winSize.width * 0.7, 0, cc.winSize.width * 0.3, cc.winSize.height * 0.4));
        this.joystick.setOpacity(50);
        this.addChild(this.joystick, 1);

        return true;
    },
    enable:function() {
        this.joystick.enable();
    },
    disable:function() {
        this.joystick.disable();
    },
    getVelocity:function() {
        return this.joystick.getVelocityUnitVector();
    },
    getDegrees:function() {
        return this.joystick.getAngleInDegrees();
    },
    isTriggered:function() {
        return this.joystick.isTriggered();
    }
});
