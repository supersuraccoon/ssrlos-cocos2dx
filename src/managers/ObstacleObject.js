
var ObstacleObject = ssr.EditablePolyNode.extend({
    ctor:function(id, vertexArray, isPolygon) {
        this._super(vertexArray, isPolygon);
        this.setID(id);
    },  
    setID:function(id) {
        this._id = id;
    },
    getID:function() {
        return this._id;
    }
});

var ObstacleObjectManager = cc.Class.extend({
    init:function() {
        this._obstacleObjectMap = {};
        //
        this._delegate = null;
        this._obstacleObjectID = 0;
        return true;
    },
    getFreeID:function() {
        return ++this._obstacleObjectID;
    },
    createObstacle:function(vertexArray, isPolygon) {
        var obstacleId = this.getFreeID();
        var obstacleNode = new ObstacleObject(obstacleId, vertexArray, isPolygon);
        this._obstacleObjectMap[obstacleId] = obstacleNode;
        if (obstacleNode.setDelegate) {
            obstacleNode.setDelegate(this);
        }
        return obstacleNode;
    },
    getObstacleArray:function() {
        var self = this;
        var obstacleArray = [];
        for (var obstacleId in this._obstacleObjectMap) {
            obstacleArray.push(this._obstacleObjectMap[obstacleId]);
        }
        return obstacleArray;
    },
    findObstacleById:function(obstacleId) {
        return this._obstacleObjectMap[obstacleId];
    },
    removeObstacle:function(obstacleNode) {
        if (obstacleNode) {
            if (obstacleNode.getParent()) {
                obstacleNode.removeFromParent(true);
            }
            delete this._obstacleObjectMap[obstacleNode.getID()];
            return true;
        }
        else {
            return false;
        }
    },
    removeObstacles:function(obstacleNodes) {
        for (var i = 0; i < obstacleNodes.length; i ++) {
            this.removeObstacle(obstacleNodes[i]);
        }
    },
    removeObstacleById:function(obstacleId) {
        var obstacleNode = this.findObstacleById(obstacleId);
        if (obstacleNode) {
            return this.removeObstacle(obstacleNode);
        }
        else {
            return false;
        }
    },
    removeAllObstacles:function() {
        for (var obstacleId in this._obstacleObjectMap) {
            this.removeObstacle(this._obstacleObjectMap[obstacleId]);
        }
        this._obstacleObjectMap = {};
    },
    // delegate && callback
    setDelegate:function(delegate) {
        this._delegate = delegate;
    },
    removeCallback:function(node) {
        this.removeObstacle(node);
        if (this._delegate && this._delegate.obstacleRemoveCallback) {
            this._delegate.obstacleRemoveCallback(node);
        }
    },
    moveCallback:function(node) {
        if (this._delegate && this._delegate.obstacleUpdatedCallback) {
            this._delegate.obstacleUpdatedCallback(node);
        }
    },
    vertexUpdateCallback:function(node, vertexIndex, oldPosition, newPosition) {
        if (this._delegate && this._delegate.obstacleVertexUpdateCallback) {
            this._delegate.obstacleVertexUpdateCallback(node, vertexIndex, oldPosition, newPosition);
        }
    },
    rotateCallback:function(node) {
        if (this._delegate && this._delegate.obstacleUpdatedCallback) {
            this._delegate.obstacleUpdatedCallback(node);
        }
    },
    scaleCallback:function(node) {
        if (this._delegate && this._delegate.obstacleUpdatedCallback) {
            this._delegate.obstacleUpdatedCallback(node);
        }
    }
});

var sharedObstacleObjectManager = null;
ObstacleObjectManager.getInstance = function () {
    if (sharedObstacleObjectManager == null) {
        sharedObstacleObjectManager = new ObstacleObjectManager();
        sharedObstacleObjectManager.init();
    }
    return sharedObstacleObjectManager;
}

// var ObstacleObject = cc.Node.extend({
//     ctor:function() {
//         this._super();
//         //
//         this._renderNode = new cc.DrawNode();
//         this.addChild(this._renderNode);
//         this.setAnchorPoint(0.5, 0.5);
//         //
//         this._render();
//     },
//     _render:function() {
//         this._renderNode.clear();
//         var sides = 6;
//         var size = 16;
//         this.vertArray = [];
//         for (var i = 0; i < sides; i++) {
//             this.vertArray.push(cc.p(
//                 size * Math.cos(i / sides * Math.PI * 2),
//                 size * Math.sin(i / sides * Math.PI * 2))
//             );
//         }
//         this._renderNode.drawPoly(this.vertArray, cc.color.WHITE, cc.FLT_EPSILON, cc.color(0, 0, 0, 0));
//     },
//     getVertexArray:function() {
//         var result = [];
//         var anAffineTransform = this.getNodeToParentTransform();
//         for (var i = 0; i < this.vertArray.length; i ++) {
//             var transformed = cc.pointApplyAffineTransform(this.vertArray[i], anAffineTransform);
//             result.push(transformed);
//         }
//         return result;
//     }
// });

