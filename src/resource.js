var res = {
    robot_png                                : "res/robot.png",
    joystick_png                             : "res/joystick.png",
    joystick_plist                           : "res/joystick.plist",
    flag_png                                 : "res/flag.png",
    gridLine_png                             : "res/gridLine.png",
    gridLineSmall_png                        : "res/gridLineSmall.png",
    shadow_png                        		 : "res/shadow.png"
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
