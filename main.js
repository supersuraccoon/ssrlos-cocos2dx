cc.game.MODULES = {
	"SSRNamespace" : 
	[
		"src/ssr-kit/SSRKit.js"
	],
	"SSRAddon" : 
	[
		"src/ssr-kit/addon/SSRAddon.js",
		"src/ssr-kit/addon/SSRTouchableAddon.js"
	],
	"SSRPolyHelper" : 
	[
		"src/ssr-kit/poly-helper/SSRPolyHelper.js"
	],
	"SSRJoystick" : 
	[
		"src/ssr-kit/joystick/SSRJoystick.js"
	],
	"SSRPanel" : 
	[
		"src/ssr-kit/panel/SSRPanel.js",
		"src/ssr-kit/panel/SSRScrollPanel.js",
		"src/ssr-kit/panel/SSRMenuPanelItem.js",
		"src/ssr-kit/panel/SSRToggleMenuPanelItem.js",
		"src/ssr-kit/panel/SSRTextPanelItem.js"
	],
	"SSREditableGeometryNode" : 
	[
		"src/ssr-kit/editable-geometry-node/SSREditableGeometryNode.js",
		"src/ssr-kit/editable-geometry-node/SSREditablePolyNode.js"
	],
	"SSRLoS" : 
	[
		// "src/ssr-kit/line-of-sight/SSRLoS.min.js",
        "src/ssr-kit/line-of-sight/namespace/SSRLoSNamespace.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSConstant.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataPool.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataManager.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataEdge.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataAnglePoint.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataRay.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataHitPoint.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataBoundary.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataBoundaryNull.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataBoundaryRectangle.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataBoundaryCircle.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataBoundarySector.js",
        "src/ssr-kit/line-of-sight/data/SSRLoSDataObstacle.js",
        "src/ssr-kit/line-of-sight/helper/SSRLoSHelper.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessBase.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessUnlimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessLimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessLimitedRangeWithFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessLimitedRangeWithNonFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessLimitedRangeWithReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/pre-process/SSRLoSStrategyPreProcessLimitedRangeWithNonReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingBase.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingUnlimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingLimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingLimitedRangeWithFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingLimitedRangeWithNonFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingLimitedRangeWithReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/culling/SSRLoSStrategyCullingLimitedRangeWithNonReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessBase.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessUnlimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessLimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessLimitedRangeWithFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessLimitedRangeWithNonFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessLimitedRangeWithReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/process/SSRLoSStrategyProcessLimitedRangeWithNonReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessBase.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessUnlimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessLimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessLimitedRangeWithFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessLimitedRangeWithNonFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessLimitedRangeWithReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/post-process/SSRLoSStrategyPostProcessLimitedRangeWithNonReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolBase.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolUnlimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolLimitedRange.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolLimitedRangeWithFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolLimitedRangeWithNonFullAngle.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolLimitedRangeWithReflexAngle.js",
        "src/ssr-kit/line-of-sight/strategy/tool/SSRLoSStrategyToolLimitedRangeWithNonReflexAngle.js",
        "src/ssr-kit/line-of-sight/render/SSRLoSRenderUtil.js",
        "src/ssr-kit/line-of-sight/render/SSRLoSRenderLight.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentCore.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentMask.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentShadow.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderBase.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderBlockingEdge.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderHitPoint.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderPotentialBlockingEdge.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderRay.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderSightArea.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderSightLight.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderSightRange.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderSightVert.js",
        "src/ssr-kit/line-of-sight/component/SSRLoSComponentRenderVisibleEdge.js",
        "src/ssr-kit/line-of-sight/config/SSRLoSConfig.js"
	],
	"SSRKeyboardController" : 
	[
		"src/ssr-kit/keyboard-controller/SSRKeyboardController.js"
	],
	"Demo" 	   : 
	[
		// resource
		"src/resource.js",
		// misc
		"src/misc/saveAs.js",
		// tests
	    "src/tests/TestLoader.js",
	    "src/tests/TestCullingLoSBase.js",
	    "src/tests/TestUnlimitedRangeLoS.js",
	    "src/tests/TestLimitedRangeWithFullAngleLoS.js",
	    "src/tests/TestLimitedRangeWithNonReflexAngleLoS.js",
	    "src/tests/TestLimitedRangeWithReflexAngleLoS.js",
	    "src/tests/TestLoSMapBase.js",
	    "src/tests/TestLoSPlayground.js",
	    "src/tests/TestLoSPerformance.js",
	    // renders
	    "src/renders/RobotObject.js",
	    "src/renders/LightObject.js",
	    // managers
	    "src/managers/ObstacleObject.js",
	    // layers
	    "src/layers/JoyStickLayer.js",
	    // demo
	    "src/layers/Demo.js"
	]
};

cc.game.SCRIPT_MODULES = [
	"SSRNamespace",
	"SSRAddon",
	"SSRPolyHelper",
	"SSRJoystick",
	"SSREditableGeometryNode",
	"SSRPanel",
	"SSRLoS",
	"SSRKeyboardController",
	"Demo"
];

cc.game.onStart = function() {
	// view setting
	cc.view.setOrientation(cc.ORIENTATION_LANDSCAPE);
    cc.view.setDesignResolutionSize(1136, 640, cc.ResolutionPolicy.FIXED_HEIGHT);
    cc.view.resizeWithBrowserSize(true);
    // parse && load game scripts
    if (USE_PROJECT_JSON) {
    	cc.LoaderScene.preload(g_resources, function () {
	        cc.director.runScene(new DemoScene());
	    }, this);
    }
    else {
    	var scripts = [];
	    for (var i = 0; i < cc.game.SCRIPT_MODULES.length; i ++) {
	    	var moduleName = cc.game.SCRIPT_MODULES[i];
	    	for (var j = 0; j < cc.game.MODULES[moduleName].length; j ++) {
	    		scripts.push(cc.game.MODULES[moduleName][j]);
	    	}
	    }
	    if (cc.sys.isNative) {
	    	for (var i = 0; i < scripts.length; i ++) {
                console.log(scripts[i]);
	    		require(scripts[i]);
	    	}
	    	cc.director.runScene(new DemoScene());
	    }
	    else {
	    	var self = this;
		    cc.loader.loadJs("", scripts,
		    	function () {
			        cc.LoaderScene.preload(g_resources, function () {
				        cc.director.runScene(new DemoScene());
				    }, self);
			    }
		    );
	    }
    }
};

var USE_PROJECT_JSON = true;

cc.game.run( USE_PROJECT_JSON ? null : {
    "project_type" 			: "javascript",
    "debugMode" 			: cc.game.DEBUG_MODE_NONE,
    "showFPS"  				: true,
    "frameRate"  			: 60,
    "noCache"  				: true,
    "id"  					: "gameCanvas",
    "renderMode" 			: 2,
    "engineDir" 			: "frameworks/cocos2d-html5",
    "modules" 				: ["core", "menus", "gui", "base4webgl", "shape-nodes"]
});
